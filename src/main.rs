use crate::exercises::a_julia::a_julia;
use crate::exercises::a_cameras::a_cameras;
use crate::exercises::a_solids::a_solids;

mod core;
mod rt;
mod exercises;

fn main() {
    a_julia();
    //a_cameras();
    //a_solids();
}
