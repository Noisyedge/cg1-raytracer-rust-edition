use std::ops::{Add, Mul};

type Mul32<T> = <T as Mul<f32>>::Output;
type LerpRet<T> = <Mul32<T> as Add>::Output;


pub fn lerp<T>(px0: T, px1: T, x_point: f32) -> LerpRet<T>
    where T: Mul<f32>,
          Mul32<T>: Add
{
    px0 * (1.0 - x_point) + px1 * x_point
}

pub fn lerpbar<T>(a: T, b: T, c: T, a_weight: f32, b_weight: f32)
                  -> <LerpRet<T> as Add<Mul32<T>>>::Output
    where T: Mul<f32>,
          Mul32<T>: Add,
          LerpRet<T>: Add<Mul32<T>>
{
    a * a_weight + b * b_weight + c * ((1.0 - a_weight - b_weight) as f32)
}


pub fn lerp2d<T>(px0y0: T, px1y0: T, px0y1: T, px1y1: T, x_weight: f32, y_weight: f32)
                 -> LerpRet<LerpRet<T>>
    where T: Mul<f32>,
          <T as Mul<f32>>::Output: Add,
          <<T as Mul<f32>>::Output as Add>::Output: Mul<f32>,
          <<<T as Mul<f32>>::Output as Add>::Output as Mul<f32>>::Output: Add

{
    let a = lerp(px0y0, px1y0, x_weight);
    let b = lerp(px0y1, px1y1, x_weight);
    lerp(a, b, y_weight)
}


pub fn lerp3d<T>(px0y0z0: T, px1y0z0: T, px0y1z0: T, px1y1z0: T,
                 px0y0z1: T, px1y0z1: T, px0y1z1: T, px1y1z1: T,
                 x_point: f32, y_point: f32, z_point: f32)
                 -> LerpRet<LerpRet<LerpRet<T>>>
    where T: Mul<f32>,
          Mul32<T>: Add,
          LerpRet<T>: Mul<f32>,
          Mul32<LerpRet<T>>: Add,
          LerpRet<LerpRet<T>>: Mul<f32>,
          Mul32<LerpRet<LerpRet<T>>>: Add
{
    let a = lerp2d(px0y0z0, px0y1z0, px0y0z1, px0y1z1, y_point, z_point);
    let b = lerp2d(px1y0z0, px1y1z0, px1y0z1, px1y1z1, y_point, z_point);
    lerp(a, b, x_point)
}

