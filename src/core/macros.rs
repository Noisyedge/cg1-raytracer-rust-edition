#[macro_export]
macro_rules! impl_component_operations {
    ($struct:tt, $member_type:ty, $minus_type:tt, $($member:ident),+) => {
        impl $struct{
            pub const fn new($($member:$member_type,)+) -> $struct {
                $struct{
                    $($member,)+
                }
            }

            #[inline]
            pub const fn rep(x:$member_type) -> $struct {
                $struct{
                    $($member: x,)+
                }
            }

            #[inline]
            pub fn min(&self, rhs:$struct) -> $struct{
                $struct::new($(if self.$member < rhs.$member{self.$member}else{rhs.$member},)+)
            }

            #[inline]
            pub fn max(&self, rhs:$struct) -> $struct{
                $struct::new($(if self.$member > rhs.$member{self.$member}else{rhs.$member},)+)
            }
        }

        impl std::ops::Add for $struct {
            type Output = $struct;

            #[inline]
            fn add(self, rhs: $struct) -> Self::Output{
                $struct{
                    $(
                        $member: self.$member + rhs.$member,
                    )+
                }
            }
        }

        impl std::ops::Add for &$struct {
            type Output = $struct;

            #[inline]
            fn add(self, rhs: &$struct) -> $struct{
                $struct{
                    $(
                        $member: self.$member + rhs.$member,
                    )+
                }
            }
        }

        impl std::ops::Sub for $struct {
            type Output = $minus_type;

            #[inline]
            fn sub(self, rhs: $struct) -> Self::Output{
                $minus_type{
                    $(
                        $member: self.$member - rhs.$member,
                    )+
                }
            }
        }

        impl std::ops::Sub for &$struct {
            type Output = $minus_type;

            #[inline]
            fn sub(self, rhs: &$struct) -> $minus_type{
                $minus_type{
                    $(
                        $member: self.$member - rhs.$member,
                    )+
                }
            }
        }

        impl std::ops::Mul for $struct {
            type Output = $struct;

            #[inline]
            fn mul(self, rhs: $struct) -> Self::Output{
                $struct{
                    $(
                        $member: self.$member * rhs.$member,
                    )+
                }
            }
        }

        impl std::ops::Mul for &$struct {
            type Output = $struct;

            #[inline]
            fn mul(self, rhs: &$struct) -> $struct{
                $struct{
                    $(
                        $member: self.$member * rhs.$member,
                    )+
                }
            }
        }

        impl std::ops::Neg for $struct {
            type Output = $struct;

            #[inline]
            fn neg(self) -> Self::Output{
                $struct{
                    $(
                        $member: -self.$member,
                    )+
                }
            }
        }

        impl std::ops::Neg for &$struct {
            type Output = $struct;

            #[inline]
            fn neg(self) -> $struct{
                $struct{
                    $(
                        $member: -self.$member,
                    )+
                }
            }
        }

        impl std::cmp::PartialEq for $struct {

            #[inline]
            fn eq(&self, rhs: &$struct) -> bool{
                true $( && (self.$member == rhs.$member) )+

            }
        }

        impl std::ops::Mul<f32> for $struct {
            type Output = $struct;

            #[inline]
            fn mul(self, rhs:f32) -> Self::Output{
                $struct{
                    $(
                        $member: self.$member * rhs,
                    )+
                }
            }
        }

        impl std::ops::Mul<f32> for &$struct {
            type Output = $struct;

            #[inline]
            fn mul(self, rhs: f32) -> $struct{
                $struct{
                    $(
                        $member: self.$member * rhs,
                    )+
                }
            }
        }

        impl std::ops::Mul<$struct> for f32 {
            type Output = $struct;

            #[inline]
            fn mul(self, rhs:$struct) -> $struct{
                $struct{
                    $(
                        $member: rhs.$member * self,
                    )+
                }
            }
        }

        impl std::ops::Mul<&$struct> for f32 {
            type Output = $struct;

            #[inline]
            fn mul(self, rhs: &$struct) -> $struct{
                $struct{
                    $(
                        $member: rhs.$member * self,
                    )+
                }
            }
        }

        impl std::ops::Div<f32> for $struct {
            type Output = $struct;

            #[inline]
            fn div(self, rhs:f32) -> Self::Output{
                $struct{
                    $(
                        $member: self.$member /rhs,
                    )+
                }
            }
        }

        impl std::ops::Div<f32> for &$struct {
            type Output = $struct;

            #[inline]
            fn div(self, rhs: f32) -> $struct{
                let r: f32 = 1.0 / rhs;
                $struct{
                    $(
                        $member: self.$member * r,
                    )+
                }
            }
        }

    }
}

















