use crate::core::point::Point;
use crate::core::vector::*;

const NUM_ITER: u32 = 512;

pub fn julia(v: Point, c: Point) -> u32{
    let mut p = v - Point::rep(0.0);
    let mut i = 0;
    while i < NUM_ITER{
        if p.lensqr() > 1.0e+8{
            break;
        }
        let mut qs = p;
        qs.y = -p.y;
        p = Vector::new(dot(qs,p), cross(qs,p).z, 0.0) + c - Point::rep(0.0);
        i += 1;
    }
    i
}
