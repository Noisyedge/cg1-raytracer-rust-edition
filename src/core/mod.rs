#[macro_use]
pub mod macros;
pub mod color;
pub mod float4;
pub mod image;
pub mod interpolate;
pub mod julia;
pub mod matrix;
pub mod point;
pub mod scalar;
pub mod sobel;
pub mod vector;