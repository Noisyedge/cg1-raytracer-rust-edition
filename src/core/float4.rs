use crate::core::vector::Vector;
use crate::core::point::Point;
use std::ops::Index;
use std::ops::IndexMut;

#[derive(Clone, Copy, Debug)]
pub struct Float4 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w: f32
}

impl_component_operations!(Float4, f32, Float4, x,y,z,w);

impl Float4{
    pub const fn from_vector(v: Vector) -> Float4{
        Float4::new(v.x, v.y, v.z, 0.0)
    }

    pub const fn from_point(p: Point) -> Float4{
        Float4::new(p.x, p.y, p.z, 1.0)
    }

    pub const fn to_vector(&self) -> Vector{
        Vector::new(self.x, self.y, self.z)
    }

    pub const fn to_point(&self) -> Point{
        Point::new(self.x, self.y, self.z)
    }
}

impl From<[f32;4]> for Float4{
    fn from(arr: [f32;4]) -> Float4{
        Float4::new(arr[0], arr[1], arr[2], arr[3])
    }
}

impl Index<usize> for Float4{
    type Output = f32;

    fn index(&self, i: usize) -> &f32{
        match i {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            3 => &self.w,
            _ => panic!("Out of bounds, expected 0-3, got {:?}", i)
        }
    }
}

impl IndexMut<usize> for Float4{
    fn index_mut(&mut self, i: usize) -> &mut f32{
        match i {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            3 => &mut self.w,
            _ => panic!("Out of bounds, expected 0-3, got {:?}", i)
        }
    }
}