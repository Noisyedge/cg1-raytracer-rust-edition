use std::ops::Index;
use std::ops::IndexMut;

#[derive(Clone, Copy, Debug, Default)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

impl Vector {
    #[inline]
    pub fn normalize(&self) -> Vector {
        let len = self.length();
        if len == 0.0 {
            *self
        } else {
            self / len
        }
    }

    #[inline]
    pub fn lensqr(&self) -> f32 {
        (self.x * self.x + self.y * self.y + self.z * self.z)
    }

    #[inline]
    pub fn length(&self) -> f32 {
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    #[inline]
    pub fn from_float4() {
        unimplemented!()
    }
}

impl Index<usize> for Vector {
    type Output = f32;

    #[inline]
    fn index(&self, i: usize) -> &f32 {
        match i {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => panic!("Index is out of range, expected 0-3, got {:?}", i)
        }
    }
}

impl IndexMut<usize> for Vector {

    #[inline]
    fn index_mut(&mut self, i: usize) -> &mut f32 {
        match i {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            _ => panic!("Index is out of range, expected 0-3, got {:?}", i)
        }
    }
}

#[inline]
pub fn cross(a: Vector, b: Vector) -> Vector {
    Vector::new(
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x,
    )
}

#[inline]
pub fn dot(a: Vector, b: Vector) -> f32 {
    a.x * b.x + a.y * b.y + a.z * b.z
}

#[inline]
pub fn reflect(I: Vector, n: Vector) -> Vector {
    -I + 2.0 * (dot(I, n)) * n
}


pub fn orth(v: &Vector) -> Vector {
    let mx = v.x.abs();
    let my = v.y.abs();
    let mz = v.z.abs();

    let  sx;
    let  sy;
    let  sz ;
    if mx <= my && mx <= mz {
        sx = 0.0;
        sy = -v.z;
        sz = v.y;
    } else if my <= mx && my <= mz {
        sx = -v.z;
        sy = 0.0;
        sz = v.x;
    } else {
        sx = -v.y;
        sy = v.x;
        sz = 0.0;
    }

    Vector::new(sx, sy, sz)
}

impl_component_operations!(Vector, f32, Vector, x, y, z);