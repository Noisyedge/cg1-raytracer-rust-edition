use std::ops::Index;
use crate::core::float4::Float4;
use std::ops::IndexMut;
use std::ops::Add;
use std::ops::Sub;
use std::ops::Mul;
use crate::core::vector::Vector;
use crate::core::point::Point;
use crate::core::vector::cross;

#[derive(Clone, Debug, PartialEq)]
pub struct Matrix([Float4; 4]);

impl Matrix {
    pub const fn new(a: Float4, b: Float4, c: Float4, d: Float4) -> Matrix {
        Matrix([a, b, c, d])
    }


    pub fn transpose(&self) -> Matrix {
        Matrix::new(
            Float4::new(self[0][0], self[1][0], self[2][0], self[3][0]),
            Float4::new(self[0][1], self[1][1], self[2][1], self[3][1]),
            Float4::new(self[0][2], self[1][2], self[2][2], self[3][2]),
            Float4::new(self[0][3], self[1][3], self[2][3], self[3][3])
        )
    }

    pub fn invert(&self) -> Matrix {
        let mut result = Matrix::zero();
        let m = self;

        //Taken and modified from http://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix
        result[0][0] = m[1][1] * m[2][2] * m[3][3] - m[1][1] * m[2][3] * m[3][2] - m[2][1] * m[1][2] * m[3][3] +
            m[2][1] * m[1][3] * m[3][2] + m[3][1] * m[1][2] * m[2][3] - m[3][1] * m[1][3] * m[2][2];
        result[1][0] = -m[1][0] * m[2][2] * m[3][3] + m[1][0] * m[2][3] * m[3][2] + m[2][0] * m[1][2] * m[3][3] -
            m[2][0] * m[1][3] * m[3][2] - m[3][0] * m[1][2] * m[2][3] + m[3][0] * m[1][3] * m[2][2];
        result[2][0] = m[1][0] * m[2][1] * m[3][3] - m[1][0] * m[2][3] * m[3][1] - m[2][0] * m[1][1] * m[3][3] +
            m[2][0] * m[1][3] * m[3][1] + m[3][0] * m[1][1] * m[2][3] - m[3][0] * m[1][3] * m[2][1];
        result[3][0] = -m[1][0] * m[2][1] * m[3][2] + m[1][0] * m[2][2] * m[3][1] + m[2][0] * m[1][1] * m[3][2] -
            m[2][0] * m[1][2] * m[3][1] - m[3][0] * m[1][1] * m[2][2] + m[3][0] * m[1][2] * m[2][1];

        let det = m[0][0] * result[0][0] + m[0][1] * result[1][0] + m[0][2] * result[2][0] + m[0][3] * result[3][0];
        if det == 0.0 {
            return Matrix::zero();
        }

        result[0][1] = -m[0][1] * m[2][2] * m[3][3] + m[0][1] * m[2][3] * m[3][2] + m[2][1] * m[0][2] * m[3][3] -
            m[2][1] * m[0][3] * m[3][2] - m[3][1] * m[0][2] * m[2][3] + m[3][1] * m[0][3] * m[2][2];
        result[1][1] = m[0][0] * m[2][2] * m[3][3] - m[0][0] * m[2][3] * m[3][2] - m[2][0] * m[0][2] * m[3][3] +
            m[2][0] * m[0][3] * m[3][2] + m[3][0] * m[0][2] * m[2][3] - m[3][0] * m[0][3] * m[2][2];
        result[2][1] = -m[0][0] * m[2][1] * m[3][3] + m[0][0] * m[2][3] * m[3][1] + m[2][0] * m[0][1] * m[3][3] -
            m[2][0] * m[0][3] * m[3][1] - m[3][0] * m[0][1] * m[2][3] + m[3][0] * m[0][3] * m[2][1];
        result[3][1] = m[0][0] * m[2][1] * m[3][2] - m[0][0] * m[2][2] * m[3][1] - m[2][0] * m[0][1] * m[3][2] +
            m[2][0] * m[0][2] * m[3][1] + m[3][0] * m[0][1] * m[2][2] - m[3][0] * m[0][2] * m[2][1];
        result[0][2] = m[0][1] * m[1][2] * m[3][3] - m[0][1] * m[1][3] * m[3][2] - m[1][1] * m[0][2] * m[3][3] +
            m[1][1] * m[0][3] * m[3][2] + m[3][1] * m[0][2] * m[1][3] - m[3][1] * m[0][3] * m[1][2];
        result[1][2] = -m[0][0] * m[1][2] * m[3][3] + m[0][0] * m[1][3] * m[3][2] + m[1][0] * m[0][2] * m[3][3] -
            m[1][0] * m[0][3] * m[3][2] - m[3][0] * m[0][2] * m[1][3] + m[3][0] * m[0][3] * m[1][2];
        result[2][2] = m[0][0] * m[1][1] * m[3][3] - m[0][0] * m[1][3] * m[3][1] - m[1][0] * m[0][1] * m[3][3] +
            m[1][0] * m[0][3] * m[3][1] + m[3][0] * m[0][1] * m[1][3] - m[3][0] * m[0][3] * m[1][1];
        result[3][2] = -m[0][0] * m[1][1] * m[3][2] + m[0][0] * m[1][2] * m[3][1] + m[1][0] * m[0][1] * m[3][2] -
            m[1][0] * m[0][2] * m[3][1] - m[3][0] * m[0][1] * m[1][2] + m[3][0] * m[0][2] * m[1][1];
        result[0][3] = -m[0][1] * m[1][2] * m[2][3] + m[0][1] * m[1][3] * m[2][2] + m[1][1] * m[0][2] * m[2][3] -
            m[1][1] * m[0][3] * m[2][2] - m[2][1] * m[0][2] * m[1][3] + m[2][1] * m[0][3] * m[1][2];
        result[1][3] = m[0][0] * m[1][2] * m[2][3] - m[0][0] * m[1][3] * m[2][2] - m[1][0] * m[0][2] * m[2][3] +
            m[1][0] * m[0][3] * m[2][2] + m[2][0] * m[0][2] * m[1][3] - m[2][0] * m[0][3] * m[1][2];
        result[2][3] = -m[0][0] * m[1][1] * m[2][3] + m[0][0] * m[1][3] * m[2][1] + m[1][0] * m[0][1] * m[2][3] -
            m[1][0] * m[0][3] * m[2][1] - m[2][0] * m[0][1] * m[1][3] + m[2][0] * m[0][3] * m[1][1];
        result[3][3] = m[0][0] * m[1][1] * m[2][2] - m[0][0] * m[1][2] * m[2][1] - m[1][0] * m[0][1] * m[2][2] +
            m[1][0] * m[0][2] * m[2][1] + m[2][0] * m[0][1] * m[1][2] - m[2][0] * m[0][2] * m[1][1];

        result * (1.0 / det)
    }

    pub fn det(&self) -> f32 {
        let mut result = Matrix::zero();
        let m = self;

        //Taken and modified from http://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix
        result[0][0] = m[1][1] * m[2][2] * m[3][3] - m[1][1] * m[2][3] * m[3][2] - m[2][1] * m[1][2] * m[3][3] +
            m[2][1] * m[1][3] * m[3][2] + m[3][1] * m[1][2] * m[2][3] - m[3][1] * m[1][3] * m[2][2];
        result[1][0] = -m[1][0] * m[2][2] * m[3][3] + m[1][0] * m[2][3] * m[3][2] + m[2][0] * m[1][2] * m[3][3] -
            m[2][0] * m[1][3] * m[3][2] - m[3][0] * m[1][2] * m[2][3] + m[3][0] * m[1][3] * m[2][2];
        result[2][0] = m[1][0] * m[2][1] * m[3][3] - m[1][0] * m[2][3] * m[3][1] - m[2][0] * m[1][1] * m[3][3] +
            m[2][0] * m[1][3] * m[3][1] + m[3][0] * m[1][1] * m[2][3] - m[3][0] * m[1][3] * m[2][1];
        result[3][0] = -m[1][0] * m[2][1] * m[3][2] + m[1][0] * m[2][2] * m[3][1] + m[2][0] * m[1][1] * m[3][2] -
            m[2][0] * m[1][2] * m[3][1] - m[3][0] * m[1][1] * m[2][2] + m[3][0] * m[1][2] * m[2][1];

        m[0][0] * result[0][0] + m[0][1] * result[1][0] + m[0][2] * result[2][0] + m[0][3] * result[3][0]
    }

    pub const fn zero() -> Matrix {
        Matrix::new(
            Float4::new(0.0, 0.0, 0.0, 0.0),
            Float4::new(0.0, 0.0, 0.0, 0.0),
            Float4::new(0.0, 0.0, 0.0, 0.0),
            Float4::new(0.0, 0.0, 0.0, 0.0)
        )
    }

    pub const fn identity() -> Matrix {
        Matrix::new(
            Float4::new(1.0, 0.0, 0.0, 0.0),
            Float4::new(0.0, 1.0, 0.0, 0.0),
            Float4::new(0.0, 0.0, 1.0, 0.0),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }

    pub const fn translation(v: &Vector) -> Matrix {
        Matrix::new(
            Float4::new(1.0, 0.0, 0.0, v.x),
            Float4::new(0.0, 1.0, 0.0, v.y),
            Float4::new(0.0, 0.0, 1.0, v.z),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }

    pub fn rotation(axis: &Vector, angle: f32) -> Matrix {
        let r = axis.normalize();
        let x = (r.x).abs();
        let y = (r.y).abs();
        let z = (r.z).abs();
        let ss;
        if x < y && y < z {
            ss = Vector::new(0.0, -r.z, r.y).normalize();
        } else if y < z {
            ss = Vector::new(-r.z, 0.0, r.x).normalize();
        } else {
            ss = Vector::new(-r.y, r.x, 0.0).normalize();
        }
        let tt = cross(r, ss);
        let mm = Matrix::system(r, ss, tt);
        let rr = mm.transpose();
        let rinv = rr.invert();
        let rot = Matrix::rotation_x(angle);
        product(rinv, product(rot, rr))
    }

    pub fn rotation_x(angle: f32) -> Matrix {
        Matrix::new(
            Float4::new(1.0, 0.0, 0.0, 0.0),
            Float4::new(0.0, angle.cos(), -(angle.sin()), 0.0),
            Float4::new(0.0, angle.sin(), angle.cos(), 0.0),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }

    pub fn rotation_y(angle: f32) -> Matrix {
        Matrix::new(
            Float4::new(angle.cos(), 0.0, angle.sin(), 0.0),
            Float4::new(0.0, 1.0, 0.0, 0.0),
            Float4::new(-(angle.sin()), 0.0, angle.cos(), 0.0),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }

    pub fn rotation_z(angle: f32) -> Matrix {
        Matrix::new(
            Float4::new(angle.cos(), -(angle.sin()), 0.0, 0.0),
            Float4::new(angle.sin(), angle.cos(), 0.0, 0.0),
            Float4::new(0.0, 0.0, 1.0, 0.0),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }

    pub const fn scaling(s: &Vector) -> Matrix {
        Matrix::new(
            Float4::new(s.x, 0.0, 0.0, 0.0),
            Float4::new(0.0, s.y, 0.0, 0.0),
            Float4::new(0.0, 0.0, s.z, 0.0),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }

    pub const fn scaling_uniform(s: f32) -> Matrix {
        Matrix::new(
            Float4::new(s, 0.0, 0.0, 0.0),
            Float4::new(0.0, s, 0.0, 0.0),
            Float4::new(0.0, 0.0, s, 0.0),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }

    pub const fn system(v1: Vector, v2: Vector, v3: Vector) -> Matrix {
        Matrix::new(
            Float4::new(v1.x, v2.x, v3.x, 0.0),
            Float4::new(v1.y, v2.y, v3.y, 0.0),
            Float4::new(v1.z, v2.z, v3.z, 0.0),
            Float4::new(0.0, 0.0, 0.0, 1.0)
        )
    }
}

pub fn product(a: Matrix, b: Matrix) -> Matrix {
    let mut m = Matrix::zero();
    for i in 0..4{
        for j in 0..4{
            for k in 0..4{
                m[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    m
}

pub fn transform_normal(m: &Matrix, n: &Vector) -> Vector {
    Vector::new(
        m[0][0] * n.x + m[0][1] * n.y * m[0][2] * n.z,
        m[1][0] * n.x + m[1][1] * n.y * m[1][2] * n.z,
        m[2][0] * n.x + m[2][1] * n.y * m[2][2] * n.z
    )
}

impl Index<usize> for Matrix {
    type Output = Float4;

    fn index(&self, i: usize) -> &Float4 {
        assert!(i <= 3);
        &self.0[i]
    }
}

impl IndexMut<usize> for Matrix {
    fn index_mut(&mut self, i: usize) -> &mut Float4 {
        assert!(i <= 3);
        &mut self.0[i]
    }
}

impl Add for Matrix {
    type Output = Matrix;

    fn add(self, rhs: Matrix) -> Self::Output {
        Matrix::new(self[0] + rhs[0], self[1] + rhs[1], self[2] + rhs[2], self[3] + rhs[3])
    }
}

impl Sub for Matrix {
    type Output = Matrix;

    fn sub(self, rhs: Matrix) -> Self::Output {
        Matrix::new(self[0] - rhs[0], self[1] - rhs[1], self[2] - rhs[2], self[3] - rhs[3])
    }
}



impl Mul<Vector> for Matrix {
    type Output = Vector;

    fn mul(self, rhs: Vector) -> Self::Output {
        (self * Float4::from_vector(rhs)).to_vector()
    }
}

impl Mul<Float4> for Matrix {
    type Output = Float4;

    fn mul(self, rhs: Float4) -> Self::Output {
        let mut res = Float4::new(0.0, 0.0, 0.0, 0.0);
        for i in 0..4 {
            for j in 0..4 {
                res[i] += self[i][j] * rhs[j]
            }
        }
        res
    }
}

impl Mul<Point> for Matrix {
    type Output = Point;

    fn mul(self, rhs: Point) -> Self::Output {
        (self * Float4::from_point(rhs)).to_point()
    }
}

impl Mul<f32> for Matrix {
    type Output = Matrix;

    fn mul(self, rhs: f32) -> Self::Output {
        let mut matrix = Matrix::zero();
        for i in 0..4 {
            for j in 0..4 {
                matrix[i][j] = rhs * self[i][j];
            }
        }
        matrix
    }
}

impl Mul<Matrix> for f32 {
    type Output = Matrix;

    fn mul(self, rhs: Matrix) -> Self::Output {
        let mut matrix = Matrix::zero();
        for i in 0..4 {
            for j in 0..4 {
                matrix[i][j] = self * rhs[i][j];
            }
        }
        matrix
    }
}





