use crate::core::image::Image;
use crate::core::color::{RGBColor, Channel};


const GAUSSIAN_K: [f32; 25] = [
    0.024, 0.034, 0.038, 0.034, 0.024,
    0.034, 0.049, 0.055, 0.049, 0.034,
    0.038, 0.055, 0.063, 0.055, 0.038,
    0.034, 0.049, 0.055, 0.049, 0.034,
    0.024, 0.034, 0.038, 0.034, 0.024
];


const SOBEL_X: [f32; 9] = [
    1.0, 0.0, -1.0,
    2.0, 0.0, -2.0,
    1.0, 0.0, -1.0
];

const SOBEL_Y: [f32; 9] = [
    1.0, 2.0, 1.0,
    0.0, 0.0, 0.0,
    -1.0, -2.0, -1.0
];

const JUST_SCALINGX: [f32; 9] = [
    0.0, 0.0, 0.0,
    -1.0, 0.0, 1.0,
    0.0, 0.0, 0.0
];

const JUST_SCALINGY: [f32; 9] = [
    0.0, -1.0, 0.0,
    0.0, 0.0, 0.0,
    0.0, 1.0, 0.0
];

pub fn apply_threshold(img: &mut [f32], t: u32, _w: usize, _h: usize) {
    img.iter_mut().for_each(|c| if *c <= t as f32 { *c = 255.0 } else { *c = 0.0 });
}

pub fn gradient_magnitude(result: &mut [f32], d_x: &[f32], d_y: &[f32], _w:usize,_h: usize) {
    for a in 0..result.len() {
        let x: f32 = d_x[a];
        let y: f32 = d_y[a];
        let squarex = x * x;
        let squarey = y * y;
        result[a] = (squarex * squarey).sqrt();
    }
}


pub fn get_pixel_value(img: &[f32], w: usize, h: usize, x: usize, y: usize) -> f32 {
    let mut x_cor = x + 1;
    let mut y_cor = y + 1;
    if x_cor > w {
        x_cor = (w + 1) - (x_cor - w);
    }
    if y_cor > h {
        y_cor = (h + 1) - (y_cor - h);
    }
    if x_cor < 1 {
        x_cor = 0 - x_cor + 1;
    }
    if y_cor < 1 {
        y_cor = 0 - y_cor + 1;
    }
    x_cor -= 1;
    y_cor -= 1;
    let pos = (y_cor * w) + x_cor;
    img[pos as usize]
}

pub fn convole(result: &mut [f32], img: &[f32], w: usize, h: usize, kernel: &[f32], w_k: usize, h_k: usize) {
    let a = ((w_k + 1) / 2) - 1;
    let b = ((h_k + 1) / 2) - 1;
    for y in 0..h {
        for x in 0..w {
            let mut fold = 0.0;
            for j in 0..h_k {
                for i in 0..w_k {
                    let kernelpix = kernel[(j * w_k + i) as usize];
                    let xcor = x + i - a;
                    let ycor = y + j - b;
                    let imgpix = get_pixel_value(img, w, h, xcor, ycor);
                    fold += kernelpix * imgpix;
                }
            }
            result[(y * w + x) as usize] = fold;
        }
    }
}

pub fn derivation_x_direction(result: &mut [f32], img: &[f32], w: usize, h: usize) {
    convole(result, img, w, h, &SOBEL_X, 3, 3);
}

pub fn derivation_y_direction(result: &mut [f32], img: &[f32], w: usize, h: usize) {
    convole(result, img, w, h, &SOBEL_Y, 3, 3);
}

pub fn derivation_x_direction_application(result: &mut [f32], img: &[f32], w: usize, h: usize) {
    convole(result, img, w, h, &JUST_SCALINGX, 3, 3);
}

pub fn derivation_y_direction_application(result: &mut [f32], img: &[f32], w: usize, h: usize) {
    convole(result, img, w, h, &JUST_SCALINGY, 3, 3);
}


pub fn scale_image(result: &mut [f32], img: &[f32], _w: usize, _h: usize) {
    let mut max = img[0];
    let mut min = img[0];
    for a in img {
        if *a < min {
            min = *a;
        }
        if *a > max {
            max = *a;
        }
    }
    for i in 0..result.len() {
        result[i] = ((img[i] - min) / (max - min)) * 255.0;
    }
}


fn apply_derivation(in_img: &Image, outx: &mut Image, outy: &mut Image, channel: Channel) {
    let w = in_img.width as usize;
    let h = in_img.height as usize;
    let img: Vec<f32> = in_img.iter().map(|p| p[channel] * 255.0).collect();


    let mut derive_x = vec!(0.0; w * h);
    let mut derive_y = vec!(0.0; w * h);
    derivation_x_direction_application(derive_x.as_mut_slice(), img.as_slice(), w, h);
    derivation_y_direction_application(derive_y.as_mut_slice(), img.as_slice(), w, h);

    let maxx = derive_x.iter().map(|x| x.abs()).fold(-1.0, f32::max);
    let maxy = derive_y.iter().map(|x| x.abs()).fold(-1.0, f32::max);

    for i in 0..derive_x.len() {
        outx[i] = RGBColor::rep(derive_x[i] / maxx * 50.0);
        outy[i] = RGBColor::rep(derive_y[i] / maxy * 50.0);
    }
}

pub fn sobel(in_img: &Image, out: &mut Image, treshold: u32, channel: Channel) {
    let w = in_img.width as usize;
    let h = in_img.height as usize;

    let img: Vec<f32> = in_img.iter().map(|c| c[channel] * 255.0).collect();
    let w_k = usize::min(w - (1 - (w % 2)), 5);
    let h_k = usize::min(h - (1 - (h & 2)), 5);
    let mut result = vec!(0.0; w*h);
    convole(result.as_mut_slice(), img.as_slice(),w,h, &GAUSSIAN_K, w_k, h_k);

    let mut derive_x = vec!(0.0; w*h);
    let mut derive_y = vec!(0.0; w*h);
    let mut scaled_derive_x = vec!(0.0; w*h);
    let mut scaled_derive_y = vec!(0.0; w*h);

    derivation_x_direction(derive_x.as_mut_slice(), result.as_slice(), w, h);
    derivation_y_direction(derive_y.as_mut_slice(), result.as_slice(), w, h);
    scale_image(scaled_derive_x.as_mut_slice(), derive_x.as_slice(), w, h);
    scale_image(scaled_derive_y.as_mut_slice(), derive_y.as_slice(), w, h);

    let mut magnitude = vec![0.0; w*h];
    let mut scaled_magnitude = vec![0.0; w*h];
    gradient_magnitude(magnitude.as_mut_slice(), derive_x.as_slice(), derive_y.as_slice(),w, h);
    scale_image(scaled_magnitude.as_mut_slice(), magnitude.as_slice(), w, h);

    apply_threshold(magnitude.as_mut_slice(), treshold, w, h);
    for i in 0..out.len(){
        out[i] = RGBColor::rep(magnitude[i] / 255.0);
    }



}












