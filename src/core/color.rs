use std::ops::Index;

#[derive(Copy, Clone, Debug)]
pub struct RGBColor{
    pub r: f32,
    pub g: f32,
    pub b: f32
}

#[derive(Clone,Copy, Debug)]
pub enum Channel {
    R = 0,
    G = 1,
    B = 2,
}

impl RGBColor{
    pub fn clamp(&self) -> RGBColor{
        RGBColor::new(
            f32::min(f32::max(self.r, 0.0), 1.0),
            f32::min(f32::max(self.g, 0.0), 1.0),
            f32::min(f32::max(self.b, 0.0), 1.0),
        )
    }
}

impl Index<Channel> for RGBColor{
    type Output = f32;

    fn index(&self, c: Channel) -> &f32{
        match c {
            Channel::R => &self.r,
            Channel::G => &self.g,
            Channel::B => &self.b,
        }
    }
}

impl_component_operations!(RGBColor, f32, RGBColor, r, g, b);