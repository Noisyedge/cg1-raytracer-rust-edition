pub fn fractional(f: f32) -> f32 {
    let (_, i) = float_extras::f64::modf(f64::from(f));
    i as f32
}

pub fn abs_fractional(f: f32) -> f32 {
    let mut fr = fractional(f);
    if fr >= 0.0 {
        fr
    } else {
        fr += 1.0;
        if fr >= 1.0 {
            0.0
        } else {
            fr
        }
    }
}

pub fn midnight_formula(a: f32, b: f32, c: f32, x0: &mut f32, x1: &mut f32) -> bool {
    let p = b * b - 4.0 * a * c;
    if p < 0.0 {
        return false;
    } else if p == 0.0 {
        *x0 = -0.5 * b / a;
        *x1 = *x0;
    } else {
        let qq = if b > 0.0 { -0.5 * (b + p.sqrt()) } else { -0.5 * (b - p.sqrt()) };
        *x0 = qq / a;
        *x1 = c / qq;
    }
    true
}

pub const SBIAS: f32 = 1e-4;







