use crate::core::color::RGBColor;
use std::ops::Index;
use std::ops::Deref;
use std::ops::IndexMut;

#[derive(Clone, Debug)]
pub struct Image {
    pub width: u32,
    pub height: u32,
    pixels: Vec<RGBColor>
}

impl Image {
    pub fn get_pixel(&self, x: u32, y: u32) -> &RGBColor {
        &self.pixels[(self.width * y + x) as usize]
    }

    pub fn set_pixel(&mut self, x: u32, y: u32, c: RGBColor) {
        self.pixels[(self.width * y + x) as usize] = c;
    }

    pub fn new(width: u32, height: u32) -> Image {
        Image {
            width,
            height,
            pixels: vec![RGBColor::rep(0.0); (width * height) as usize]
        }
    }


    pub fn clear(&mut self, color: &RGBColor) {
        self.pixels.iter_mut().for_each(|c| *c = *color);
    }

    pub fn save_png(&self, path: &str) {
        let mut buff: Vec<u8> = Vec::with_capacity(self.width as usize * self.height as usize * 3);
        self.pixels.iter().map(|c|c.clamp()).for_each(|c| {
            buff.push((c.r * 255.0).round() as u8);
            buff.push((c.g * 255.0).round() as u8);
            buff.push((c.b * 255.0).round() as u8);
        });
        image::save_buffer(path, buff.as_slice(), self.width, self.height, image::ColorType::RGB(8)).unwrap();
    }

    pub fn read_png(&mut self, path: &str) {
        let img = image::open(path).unwrap();
        let buf = img.as_rgb8().unwrap();
        self.width = buf.width();
        self.height = buf.height();
        self.pixels = Vec::with_capacity(self.width as usize * self.height as usize);
        for p in buf.pixels() {
            self.pixels.push(RGBColor::new(f32::from(p.data[0]) / 255.0, f32::from(p.data[1]) / 255.0, f32::from(p.data[2]) / 255.0))
        }
    }
}

impl IndexMut<usize> for Image {
    fn index_mut(&mut self, i: usize) -> &mut RGBColor {
        &mut self.pixels[i]
    }
}

impl Index<usize> for Image {
    type Output = RGBColor;
    fn index(&self, i: usize) -> &RGBColor {
        &self.pixels[i]
    }
}

impl Deref for Image {
    type Target = [RGBColor];

    fn deref(&self) -> &[RGBColor] {
        self.pixels.as_slice()
    }
}

