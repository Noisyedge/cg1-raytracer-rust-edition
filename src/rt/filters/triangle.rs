use crate::rt::filters::filter::Filter;

pub struct TriangleFilter {
    rx: f32,
    ry: f32
}

pub const tf: TriangleFilter = TriangleFilter { rx: 0.75, ry: 0.75 };

impl TriangleFilter {
    pub fn new(rx: f32, ry: f32) -> TriangleFilter {
        TriangleFilter { rx, ry }
    }
}


impl Filter for TriangleFilter {
    fn evaluate(&self, x: f32, y: f32) -> f32 {
        f32::max(0.0, self.rx - x.abs()) * f32::max(0.0, self.ry - y.abs())
    }
}