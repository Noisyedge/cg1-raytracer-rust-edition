use crate::rt::filters::filter::Filter;

pub struct MitchellFilter {
    rx: f32,
    ry: f32,
    inv_rx: f32,
    inv_ry: f32,
    B: f32,
    C: f32
}

impl MitchellFilter {
    pub fn new(rx: f32, ry: f32, B: f32, C: f32) -> MitchellFilter {
        MitchellFilter {
            rx,
            ry,
            inv_rx: 1.0 / rx,
            inv_ry: 1.0 / ry,
            B,
            C
        }
    }

    fn mitchell_1d(&self, a: f32) -> f32 {
        let x = (2.0 * a).abs();
        if x > 1.0 {
            ((-self.B - 6.0 * self.C) * x * x * x + (6.0 * self.B + 30.0 * self.C) * x * x +
                (-12.0 * self.B - 48.0 * self.C) * x + (8.0 * self.B + 24.0 * self.C)) * (1.0 / 6.0)
        } else {
            ((12.0 - 9.0 * self.B - 6.0 * self.C) * x * x * x +
                (-18.0 + 12.0 * self.B + 6.0 * self.C) * x * x +
                (6.0 - 2.0 * self.B)) * (1.0 / 6.0)
        }
    }
}

impl Filter for MitchellFilter {
    fn evaluate(&self, x: f32, y: f32) -> f32 {
        self.mitchell_1d(x * self.inv_rx) * self.mitchell_1d(y * self.inv_ry)
    }
}