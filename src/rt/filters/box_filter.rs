use crate::rt::filters::filter::Filter;

pub struct BoxFilter {
    rx: f32,
    ry: f32
}

impl BoxFilter {
    pub fn new(rx: f32, ry: f32) -> BoxFilter {
        BoxFilter {
            rx,
            ry
        }
    }
}

impl Filter for BoxFilter{
    fn evaluate(&self, _x: f32, _y: f32) -> f32 {
        1.0
    }
}