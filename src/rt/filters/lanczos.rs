use std::f32::consts::PI;
use crate::rt::filters::filter::Filter;

pub struct LanczosFilter {
    rx: f32,
    ry: f32,
    tau: f32
}

impl LanczosFilter {
    pub fn new(rx: f32, ry: f32, tau: f32) -> LanczosFilter {
        LanczosFilter {
            rx,
            ry,
            tau
        }
    }

    fn sinc(&self, a: f32) -> f32 {
        let x = a.abs();
        if x < 1e-5 {
            1.0
        } else {
            (PI * x).sin() / (PI * x)
        }
    }

    fn windowed_sinc(&self, a: f32, radius: f32) -> f32 {
        let x = a.abs();
        if x > radius {
            0.0
        } else {
            let lanczos = self.sinc(x / self.tau);
            self.sinc(x) * lanczos
        }
    }
}

impl Filter for LanczosFilter {
    fn evaluate(&self, x: f32, y: f32) -> f32 {
        self.windowed_sinc(x, self.rx) * self.windowed_sinc(y, self.ry)
    }
}