use crate::rt::filters::filter::Filter;

pub struct GaussianFilter{
    rx: f32,
    ry: f32,
    alpha: f32,
    expX: f32,
    expY: f32
}

impl GaussianFilter{
    pub fn new(rx: f32, ry: f32, alpha: f32) -> GaussianFilter{
        GaussianFilter{
            rx,
            ry,
            alpha,
            expX: (-alpha * rx * rx).exp(),
            expY: (-alpha * ry * ry).exp()
        }
    }

    fn gaussian(&self, d: f32, expv: f32) -> f32{
        f32::max(0.0, (-self.alpha * d * d).exp() - expv)
    }
}

impl Filter for GaussianFilter{
    fn evaluate(&self, x: f32, y: f32) -> f32 {
        self.gaussian(x, self.expX) * self.gaussian(y, self.expY)
    }
}