pub mod box_filter;
pub mod filter;
pub mod gaussian;
pub mod lanczos;
pub mod mitchell;
pub mod triangle;