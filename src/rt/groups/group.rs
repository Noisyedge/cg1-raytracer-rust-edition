use crate::rt::primitive::Primitive;


pub trait Group<'a>: Primitive {
    fn rebuildIndex(&mut self);

    fn add(&mut self, p: &'a mut Primitive) {
        self.get_primitives_mut().push(p);
    }

    fn add_with_id(&mut self, p: &'a mut Primitive) {
        self.get_primitives_mut().push(p);
        let c = self.get_counter_mut();
        *c += 1;
        let d = *c;
        self.get_primitives_mut().last_mut().unwrap().set_id(d);
    }

    fn map_primitives(&mut self, mapper: &Fn(&mut Primitive) -> ()) {
        self.get_primitives_mut().iter_mut().for_each(|p| mapper(*p))
    }

    fn get_primitives(&self) -> &Vec<&'a mut Primitive>;

    fn get_primitives_mut(&mut self) -> &mut Vec<&'a mut Primitive>;

    fn get_counter_mut(&mut self) -> &mut u32;
}

#[macro_export]
macro_rules! impl_primitive_fields_group {

        ($struct:ty) => {
        use crate::rt::primitive::*;
        use std::rc::Rc;
        use crate::rt::materials::material::Material;
        use crate::rt::coordmappers::coordmapper::CoordMapper;
        use crate::rt::mediums::medium::Medium;

        impl PrimitiveFields for $struct{
            fn set_material(&mut self, m: Rc< Material>){
                self.primitives.iter_mut().for_each(|p| p.set_material(m.clone()));
            }

            fn get_material(&self) -> Option<Rc< Material>>{
                if self.primitives.is_empty() {None} else {self.primitives[0].get_material()}
            }

            fn get_tex_mapper(&self) -> Option<Rc< CoordMapper>>{
                self.tex_mapper.clone()
            }

            fn set_tex_mapper(&mut self, cm: Rc< CoordMapper>){
                self.tex_mapper = Some(cm);
            }



            fn get_id(&self) -> u32{
                self.id
            }

            fn set_id(&mut self, id: u32){
                self.id = id;
                self.primitives.iter_mut().for_each(|p| p.set_id(id));
            }

            fn get_counter(&self) -> u32{
                self.counter
            }

            fn get_inside(&self) -> Option<Rc<Medium>>{
                self.inside.clone()
            }

            fn get_outside(&self) -> Option<Rc<Medium>>{
                self.outside.clone()
            }

            fn set_inside(&mut self, inside: Option<Rc< Medium>>){
                self.primitives.iter_mut().for_each(|p| p.set_inside(inside.clone()));
                self.inside = inside;
            }

            fn set_outside(&mut self, outside: Option<Rc< Medium>>){
                self.primitives.iter_mut().for_each(|p| p.set_outside(outside.clone()));
                self.outside = outside;
            }

            fn set_is_medium(&mut self, is:bool){
                self.is_medium = is;
                self.primitives.iter_mut().for_each(|p| p.set_is_medium(is));
            }

            fn get_is_medium(&self) -> bool{
                self.is_medium
            }
        }
    }
}
