use crate::rt::primitive::Primitive;
use crate::rt::groups::group::Group;
use crate::rt::ray::Ray;
use crate::rt::bbox::BBox;
use crate::rt::intersection::Intersection;

pub struct SimpleGroup<'a> {
    primitives: Vec<&'a mut Primitive>,
    counter: u32,
    tex_mapper: Option<Rc<CoordMapper>>,
    id: u32,
    inside: Option<Rc<Medium>>,
    outside: Option<Rc<Medium>>,
    is_medium: bool,
    is_root: bool,
}

impl_primitive_fields_group!(SimpleGroup<'_>);

impl<'a> SimpleGroup<'a> {
    pub fn new() -> SimpleGroup<'a> {
        SimpleGroup {
            primitives: Vec::new(),
            counter: 0,
            tex_mapper: None,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false,
            is_root: false
        }
    }
}

impl<'a> Primitive for SimpleGroup<'a> {
    fn get_bounds(&self) -> BBox {
        self.primitives.iter().fold(BBox::empty(), |mut bbox, p| {
            bbox.union(p.get_bounds());
            bbox
        })
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        let mut intersection = Intersection::failure();
        let mut bestDist = previous_best_distance;
        for primitive in &self.primitives {
            let intersection_temp = primitive.intersect(ray.clone(), bestDist);

            if !intersection_temp.failure && intersection_temp.distance > 0.0 && intersection_temp.distance <= bestDist {
                intersection = intersection_temp;
                bestDist = intersection.distance;
            }
        }
        if !self.is_root && self.id != 0 {
            intersection.id = self.id;
        }
        intersection
    }
}

impl<'a> Group<'a> for SimpleGroup<'a> {
    fn rebuildIndex(&mut self) {}

    fn get_primitives(&self) -> &Vec<&'a mut Primitive> {
        &self.primitives
    }

    fn get_primitives_mut(&mut self) -> &mut Vec<&'a mut Primitive> {
        &mut self.primitives
    }

    fn get_counter_mut(&mut self) -> &mut u32 {
        &mut self.counter
    }
}