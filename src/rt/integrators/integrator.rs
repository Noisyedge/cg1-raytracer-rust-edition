use crate::rt::ray::Ray;
use crate::core::color::RGBColor;

pub trait Integrator{
    fn get_radiance(&self, ray: Ray) -> RGBColor;
}