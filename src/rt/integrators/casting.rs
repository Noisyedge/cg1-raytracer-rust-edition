use crate::rt::integrators::integrator::Integrator;
use crate::rt::world::World;
use crate::rt::ray::Ray;
use crate::core::color::RGBColor;
use crate::core::vector::dot;
use num_traits::Float;
use crate::rt::primitive::Primitive;

pub struct CastingIntegrator<'a, 'b:'a> {
    world: &'a World<'b>
}

impl<'a, 'b:'a> CastingIntegrator<'a,'b> {
    pub fn new(world: &'a World<'b>) -> CastingIntegrator<'a,'b> {
        CastingIntegrator {
            world
        }
    }
}

impl<'a, 'b:'a> Integrator for CastingIntegrator<'a,'b> {
    fn get_radiance(&self, ray: Ray) -> RGBColor {
        let intersection = self.world.scene.intersect(ray.clone(), f32::max_value());
        if !intersection.failure {
            return RGBColor::rep(dot(-ray.d, intersection.normal()));
        }
        RGBColor::rep(0.0)
    }
}