use crate::rt::integrators::integrator::Integrator;
use crate::rt::world::World;
use crate::rt::ray::Ray;
use crate::core::color::RGBColor;
use crate::core::vector::dot;
use num_traits::Float;
use crate::rt::primitive::Primitive;

pub struct CastingDistIntegrator<'a,'b:'a> {
    world: &'a World<'b>,
    near_dist: f32,
    far_dist: f32,
    near_color: RGBColor,
    far_color: RGBColor
}

impl<'a, 'b: 'a> CastingDistIntegrator<'a,'b> {
    pub fn new(world: &'a World<'b>, near_color: RGBColor, near_dist: f32, far_color: RGBColor, far_dist: f32) -> CastingDistIntegrator<'a, 'b> {
        CastingDistIntegrator {
            world,
            near_color,
            far_color,
            near_dist,
            far_dist
        }
    }
}

impl<'a, 'b:'a> Integrator for CastingDistIntegrator<'a, 'b> {
    fn get_radiance(&self, ray: Ray) -> RGBColor {
        let intersection = self.world.scene.intersect(ray.clone(), f32::max_value());
        if !intersection.failure {
            let dist = intersection.distance;
            let interpolation;
            if dist - self.near_dist < 0.0 {
                interpolation = self.near_color;
            } else if self.far_dist - dist < 0.0 {
                interpolation = self.far_color;
            } else {
                interpolation = self.near_color + (self.far_color - self.near_color) * ((dist - self.near_dist) / (self.far_dist - self.near_dist));
            }


            let cval = dot(-ray.d, intersection.normal());
            let ccol = interpolation * cval;

            return ccol;
        }
        RGBColor::rep(0.0)
    }
}