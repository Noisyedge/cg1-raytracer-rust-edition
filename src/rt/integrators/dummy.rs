use crate::rt::integrators::integrator::Integrator;
use crate::rt::world::World;
use crate::rt::ray::Ray;
use crate::core::color::RGBColor;
use crate::core::vector::dot;
use num_traits::Float;

pub struct DummyIntegrator {}

impl DummyIntegrator {
    pub fn new() -> DummyIntegrator {
        DummyIntegrator {}
    }
}

impl Integrator for DummyIntegrator {
    fn get_radiance(&self, ray: Ray) -> RGBColor {
        RGBColor::rep(0.0)
    }
}