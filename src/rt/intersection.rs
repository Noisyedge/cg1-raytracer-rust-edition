use crate::rt::ray::Ray;
use crate::core::vector::Vector;
use crate::core::point::Point;
use crate::rt::mediums::medium::Medium;
use crate::rt::solids::solid::Solid;
use crate::rt::mediums::medium::MediumInterface;
use crate::core::vector::dot;
use std::rc::Rc;

#[derive(Clone)]
pub struct Intersection<'a> {
    pub ray: Ray,
    pub solid: Option<&'a Solid>,
    pub distance: f32,
    normal: Vector,
    uv: Point,
    pub failure: bool,
    pub id: u32,
    medium_interface: Option<MediumInterface>
}

impl<'a> Intersection<'a> {
    pub fn new(distance: f32, ray: Ray, solid: &'a Solid, normal: Vector, uv: Point) -> Intersection {
        Intersection {
            ray,
            solid: Some(solid),
            distance,
            normal,
            uv,
            failure: false,
            id: 0,
            medium_interface: None
        }
    }

    pub fn new_id(distance: f32, ray: Ray, solid: &'a Solid, normal: Vector, uv: Point, id: u32) -> Intersection<'a> {
        Intersection {
            ray,
            solid: Some(solid),
            distance,
            normal,
            uv,
            failure: false,
            id,
            medium_interface: None
        }
    }

    pub fn new_medium(distance: f32, ray: Ray, solid: &'a Solid, normal: Vector, uv: Point, id: u32, medium_interface: MediumInterface) -> Intersection<'a> {
        Intersection {
            ray,
            solid: Some(solid),
            distance,
            normal,
            uv,
            failure: false,
            id,
            medium_interface: Some(medium_interface)
        }
    }

    pub fn failure() -> Intersection<'a> {
        Intersection {
            ray: Ray::default(),
            solid: None,
            distance: 0.0,
            normal: Vector::default(),
            uv: Point::default(),
            failure: true,
            id: 0,
            medium_interface: None
        }
    }

    pub fn get_medium(&self) -> Option<Rc<Medium>> {
        self.medium_interface.clone().map(|m|m.inside)
    }

    pub fn get_next_medium(&self, v: Vector) -> Option<Rc<Medium>> {
        self.medium_interface.clone().map(| mi| if dot(v, self.normal) > 0.0 {mi.outside} else {mi.inside})
    }

    pub fn hit_point(&self) -> Point {
        self.ray.o + self.distance * self.ray.d
    }

    pub fn normal(&self) -> Vector {
        self.normal
    }

    pub fn local(&self) -> Point {
        self.uv
    }
}