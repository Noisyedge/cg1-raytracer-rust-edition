use crate::core::image::Image;
use crate::rt::filters::filter::Filter;
use crate::rt::integrators::integrator::Integrator;
use crate::rt::cameras::camera::Camera;
use crate::rt::samplers::sampler::Sampler;
use crate::exercises::a_julia::a1_compute_color;
use crate::exercises::a_cameras::a2_compute_color;
use crate::rt::samplers::regular::RegularSampler;
use crate::core::point::Point2i;
use crate::rt::samplers::sampler::*;
use crate::rt::filters::triangle::TriangleFilter;
use crate::core::color::RGBColor;
use std::collections::vec_deque::VecDeque;


pub struct Renderer<'a> {
    cam: &'a Camera,
    integrator: &'a Integrator,
    samples: usize,
    sampler: Option<&'a Sampler>,
    filter: Option<&'a Filter>,
    shutter_time: f32,
    use_sampling: bool
}

struct Tile {
    x_pos: u32,
    y_pos: u32,
    x_min: u32,
    x_max: u32,
    y_min: u32,
    y_max: u32,
}

impl Tile {
    pub fn new(x_pos: u32, y_pos: u32, x_min: u32, y_min: u32, x_max: u32, y_max: u32) -> Tile {
        Tile {
            x_max,
            x_min,
            x_pos,
            y_max,
            y_min,
            y_pos
        }
    }
}

pub const TILE_SIZE: u32 = 64;


impl<'a> Renderer<'a> {
    pub fn new(cam: &'a Camera, integrator: &'a Integrator, sampler: &'a Sampler, filter: &'a Filter) -> Renderer<'a> {
        Renderer {
            cam,
            integrator,
            samples: sampler.samples_per_pixel(),
            sampler: Some(sampler),
            filter: Some(filter),
            shutter_time: 1.0,
            use_sampling: true
        }
    }

    pub fn new_ss(cam: &'a Camera, integrator: &'a Integrator) -> Renderer<'a> {
        Renderer {
            cam,
            integrator,
            samples: 1,
            sampler: None,
            filter: None,
            shutter_time: 1.0,
            use_sampling: false
        }
    }


    pub fn set_samples(&mut self, samples: usize) {
        self.samples = samples;
    }
    pub fn set_shutter_time(&mut self, time: f32) {
        self.shutter_time = time;
    }


    pub fn render(&mut self, image: &mut Image) {
        let width = image.width;
        let height = image.height;

        let half_width = width as f32 / 2.0;
        let half_height = height as f32 / 2.0;

        /*let ntilesx = (width + TILE_SIZE - 1) / TILE_SIZE;
        let ntilesy = (height + TILE_SIZE - 1) / TILE_SIZE;

        let mut tiles = Vec::with_capacity((ntilesx * ntilesy) as usize);
        for x in 0..ntilesx {
            for y in 0..ntilesy {
                let x0 = x * TILE_SIZE;
                let x1 = u32::min(x0 + TILE_SIZE, width);
                let y0 = y * TILE_SIZE;
                let y1 = u32::min(y0 + TILE_SIZE, height);
                tiles.push(Tile::new(x, y, x0, x0, x1, y1));
            }
        }
        /*if self.use_sampling {
            self.set_samples((self.samples as f32 * self.shutter_time) as usize);
        }

        let DEFAULT_SAMPLER = RegularSampler::new(1, 2);
        let DEFAULT_FILTER = TriangleFilter::new(0.75, 0.75);

        let mut i = 0;
        while let Some(tile) = tiles.pop_front() {
            i += 1;
            let mut samplercopy = SamplerClone::clone(self.sampler.unwrap_or(&DEFAULT_SAMPLER), u64::from(tile.y_pos * ntilesx + tile.x_pos));
            for y in 0..tile.y_max {
                for x in 0..tile.x_max {
                    SamplerHelper::start_pixel(samplercopy.as_mut(), Point2i { x: x as i32, y: y as i32 });
                    let mut color = RGBColor::rep(0.0);
                    let mut sum = 0.0;
                    while {
                        let cs = samplercopy.get_camera_sample(Point2i { x: x as i32, y: y as i32 });
                        let mut ray = self.cam.get_primary_ray((cs.p.x) / halfwidth - 1.0,
                                                               -((cs.p.y) / halfheight - 1.0), cs.lens_uv.x, cs.lens_uv.y, cs.time);
                        ray.time = cs.time * self.shutter_time;
                        ray.medium = self.cam.get_medium();
                        let c = self.integrator.get_radiance(ray);
                        let weight = self.filter.unwrap_or(&DEFAULT_FILTER).evaluate(cs.p.x - (x as f32 + 0.5), cs.p.y - (y as f32 + 0.5));
                        sum += weight;
                        color = color + c * weight;
                        samplercopy.start_next_sample()
                    } {}
                    image.set_pixel(x, y, color / sum);
                }
            }
            println!("Tile {:?} of {:?}", i, ntilesx * ntilesy);
        }*/
        tiles.iter().enumerate().for_each(|(i, tile)| {
            for y in 0..tile.y_max {
                for x in 0..tile.x_max {
                    image.set_pixel(x, y, self.integrator.get_radiance(self.cam.get_primary_ray_no_blur(
                        (x as f32 + 0.5) / half_width - 1.0,
                        -((y as f32 + 0.5) / half_height - 1.0))));
                }
            }
            println!("Tile {:?} of {:?}", i, ntilesx * ntilesy);
        }
        );*/

        for y in 0..height {
            for x in 0..width {
                image.set_pixel(x, y, self.integrator.get_radiance(self.cam.get_primary_ray_no_blur(
                    (x as f32 + 0.5) / half_width - 1.0,
                    -((y as f32 + 0.5) / half_height - 1.0))));
            }
            println!("row {:?} of {:?}", y, height);
        }
    }

    pub fn test_render1(&self, image: &mut Image) {
        let width = image.width;
        let height = image.height;

        let halfwidth = width as f32 / 2.0;
        let halfheight = height as f32 / 2.0;



        let ntilesx = (width + TILE_SIZE - 1) / TILE_SIZE;
        let ntilesy = (height + TILE_SIZE - 1) / TILE_SIZE;

        let mut tiles = Vec::with_capacity((ntilesx * ntilesy) as usize);
        for x in 0..ntilesx {
            for y in 0..ntilesy {
                let x0 = x * TILE_SIZE;
                let x1 = u32::min(x0 + TILE_SIZE, width);
                let y0 = y * TILE_SIZE;
                let y1 = u32::min(y0 + TILE_SIZE, height);
                tiles.push(Tile::new(x, y, x0, x0, x1, y1));
            }
        }

        tiles.iter().enumerate().for_each(|(i, tile)| {
            for y in 0..tile.y_max {
                for x in 0..tile.x_max {
                    image.set_pixel(x, y, a1_compute_color(x, y, width, height));
                }
            }
            println!("Tile {:?} of {:?}", i, ntilesx * ntilesy);
        });

        /*for y in 0..height {
            for x in 0..width {
                image.set_pixel(x, y, a1_compute_color(x,y,width, height));
            }
            println!("row {:?} of {:?}", y, height);
        }*/
    }

    pub fn test_render2(&self, image: &mut Image) {
        let width = image.width;
        let height = image.height;

        let half_width = width as f32 / 2.0;
        let half_height = height as f32 / 2.0;

        let ntilesx = (width + TILE_SIZE - 1) / TILE_SIZE;
        let ntilesy = (height + TILE_SIZE - 1) / TILE_SIZE;

        let mut tiles = Vec::with_capacity((ntilesx * ntilesy) as usize);
        for x in 0..ntilesx {
            for y in 0..ntilesy {
                let x0 = x * TILE_SIZE;
                let x1 = u32::min(x0 + TILE_SIZE, width);
                let y0 = y * TILE_SIZE;
                let y1 = u32::min(y0 + TILE_SIZE, height);
                tiles.push(Tile::new(x, y, x0, x0, x1, y1));
            }
        }
/*
        tiles.iter().enumerate().for_each(|(i, tile)| {
            for y in 0..tile.y_max {
                for x in 0..tile.x_max {
                    image.set_pixel(x, y, a2_compute_color(self.cam.get_primary_ray_no_blur(
                        (x as f32 + 0.5) / half_width - 1.0,
                        -((y as f32 + 0.5) / half_height - 1.0))));
                }
            }
            println!("Tile {:?} of {:?}", i, ntilesx * ntilesy);
        });*/

        for y in 0..height {
            for x in 0..width {
                image.set_pixel(x, y, a2_compute_color(self.cam.get_primary_ray_no_blur(
                    (x as f32 + 0.5) / half_width - 1.0,
                    -((y as f32 + 0.5) / half_height - 1.0))));
            }
            println!("row {:?} of {:?}", y, height);
        }

    }

    pub fn set_sampler(&mut self, sampler: &'a Sampler) {
        self.sampler = Some(sampler);
    }

    pub fn set_filter(&mut self, filter: &'a Filter) {
        self.filter = Some(filter);
    }
}



