use crate::core::point::Point;
use crate::core::vector::Vector;
use crate::rt::mediums::medium::Medium;
use std::rc::Rc;

#[derive(Clone, Default)]
pub struct Ray {
    pub o: Point,
    pub d: Vector,
    pub time: f32,
    pub medium: Option<Rc<Medium>>
}

impl Ray {
    pub fn new_timeless(o: Point, d: Vector) -> Ray {
        Ray {
            o,
            d,
            time: 0.0,
            medium: None
        }
    }

    pub fn new(o: Point, d: Vector, time: f32) -> Ray {
        Ray {
            o,
            d,
            time,
            medium: None
        }
    }

    pub fn new_medium(o: Point, d: Vector, time: f32, medium: Option<Rc<Medium>>) -> Ray {
        Ray {
            o,
            d,
            time,
            medium
        }
    }

    pub fn get_point(&self, distance: f32) -> Point {
        self.o + distance * self.d
    }
}