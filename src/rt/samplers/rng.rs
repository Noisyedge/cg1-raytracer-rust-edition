use std::ops::Sub;


pub const ONE_MINUS_EPSILON: f32 = 0.999_999_94_f32;

const PCG32_DEFAULT_STATE: u64 = 0x853c_49e6_748f_ea9b_u64;
const PCG32_DEFAULT_STREAM: u64 = 0xda3e_39cb_94b9_5bdb_u64;
const PCG32_MULT: u64 = 0x5851_f42d_4c95_7f2d_u64;

#[derive(Debug, Clone)]
pub struct RNG {
    state: u64,
    inc: u64
}

impl RNG {
    pub fn new() -> RNG {
        RNG {
            state: PCG32_DEFAULT_STATE,
            inc: PCG32_DEFAULT_STREAM
        }
    }

    pub fn set_sequence(&mut self, initseq: u64) {
        self.state = 0u64;
        self.inc = (initseq << 1u64) | 1u64;
        self.uniform_u32();
        self.state += PCG32_DEFAULT_STATE;
        self.uniform_u32();
    }

    pub fn from_sequence(sequence_index: u64) -> RNG {
        let mut r = RNG::new();
        r.set_sequence(sequence_index);
        r
    }

    pub fn uniform_u32(&mut self) -> u32 {
        let oldstate = self.state;
        self.state = oldstate * PCG32_MULT + self.inc;
        let xorshifted = ((oldstate >> 18u64) ^ oldstate) >> 27u64;
        let rot = oldstate >> 59u64;
        ((xorshifted >> rot) | (xorshifted << ((!rot + 1u64) & 31))) as u32
    }

    pub fn unifrom_u32_range(&mut self, b: u32) -> u32 {
        let threshold = (!b + 1u32) % b;
        loop {
            let r = self.uniform_u32();
            if r >= threshold {
                return r;
            }
        }
    }

    pub fn uniform_f32(&mut self) -> f32 {
        f32::min(ONE_MINUS_EPSILON, self.uniform_u32() as f32 * 2.328_306_436_538_696_3e-10)
    }

    pub fn shuffle<T>(&mut self, slice: &mut [T]) {
        for i in (1..slice.len()).rev() {
            slice.swap(i, self.unifrom_u32_range(i as u32 + 1) as usize);
        }
    }

    pub fn advance(&mut self, idelta: i64) {
        let mut cur_mult = PCG32_MULT;
        let mut cur_plus = self.inc;
        let mut acc_mult = 1u64;
        let mut acc_plus = 0u64;
        let mut delta = idelta as u64;
        while delta > 0 {
            if (delta & 1) != 0 {
                acc_mult *= cur_mult;
                acc_plus = acc_plus * cur_mult + cur_plus;
            }
            cur_plus *= (cur_mult + 1);
            cur_mult *= cur_mult;
            delta /= 2;
        }
        self.state = acc_mult * self.state + acc_plus;
    }
}

impl Sub for &RNG {
    type Output = i64;

    fn sub(self, rhs: &RNG) -> i64 {
        let mut cur_mult = PCG32_MULT;
        let mut cur_plus = self.inc;
        let mut cur_state = rhs.state;
        let mut the_bit = 1u64;
        let mut distance = 0u64;
        while self.state != cur_state {
            if (self.state & the_bit) != (cur_state & the_bit) {
                cur_state = cur_state * cur_mult + cur_plus;
                distance |= the_bit;
            }

            the_bit <<= 1;
            cur_plus *= (cur_mult + 1u64);
            cur_mult *= cur_mult;
        }


        distance as i64
    }
}