use crate::core::point::Point2i;
use crate::core::point::Point2f;
use crate::rt::samplers::rng::RNG;
use crate::rt::samplers::rng::ONE_MINUS_EPSILON;

#[derive(Debug, Clone)]
pub struct StratifiedSampler {
    current_pixel: Point2i,
    current_pixel_sample_index: usize,
    samples_1d_array_sizes: Vec<usize>,
    samples_2d_array_sizes: Vec<usize>,
    sample_array_1d: Vec<Vec<f32>>,
    sample_array_2d: Vec<Vec<Point2f>>,
    array_1d_offset: usize,
    array_2d_offset: usize,
    samples_per_pixel: usize,
    samples_1d: Vec<Vec<f32>>,
    samples_2d: Vec<Vec<Point2f>>,
    current_1d_dimension: usize,
    current_2d_dimension: usize,
    rng: RNG,
    n_sampled_dimensions: u32,
    x_pixel_samples: usize,
    y_pixel_samples: usize,
    jitter_samples: bool,
}

impl SamplerClone for StratifiedSampler {
    fn clone(&self, seed: u64) -> Box<Sampler> {
        let mut ss = Box::new(
            Clone::clone(self)
        );
        ss.rng.set_sequence(seed);
        ss
    }
}

impl StratifiedSampler {
    pub fn new(x_samples: usize, y_samples: usize, jitter: bool, dimensions: u32) -> StratifiedSampler {
        StratifiedSampler {
            current_pixel: Point2i { x: 0, y: 0 },
            current_pixel_sample_index: 0,
            samples_1d_array_sizes: Vec::new(),
            samples_2d_array_sizes: Vec::new(),
            sample_array_1d: Vec::new(),
            sample_array_2d: Vec::new(),
            array_1d_offset: 0,
            array_2d_offset: 0,
            samples_per_pixel: ((x_samples * y_samples) as f64).sqrt().ceil() as usize,
            samples_1d: vec!(vec!(0.0; x_samples * y_samples); dimensions as usize),
            samples_2d: vec!(vec!(Point2f { x: 0.0, y: 0.0 }; x_samples * y_samples); dimensions as usize),
            current_1d_dimension: 0,
            current_2d_dimension: 0,
            rng: RNG::new(),
            n_sampled_dimensions: dimensions,
            x_pixel_samples: x_samples,
            y_pixel_samples: y_samples,
            jitter_samples: jitter
        }
    }
}

fn stratified_sample_1d(samp: &mut [f32], n_samples: usize, rng: &mut RNG, jitter: bool) {
    let inv_n_samples = 1.0 / n_samples as f32;
    for i in 0..n_samples {
        let delta = if jitter { rng.uniform_f32() } else { 0.5 };
        samp[i] = f32::min((i as f32 + delta) * inv_n_samples, ONE_MINUS_EPSILON);
    }
}

fn stratified_sample_2d(samp: &mut [Point2f], nx: usize, ny: usize, rng: &mut RNG, jitter: bool) {
    let dx = 1.0 / nx as f32;
    let dy = 1.0 / ny as f32;
    let mut i = 0;
    for y in 0..ny {
        for x in 0..nx {
            let jx = if jitter { rng.uniform_f32() } else { 0.5 };
            let jy = if jitter { rng.uniform_f32() } else { 0.5 };
            samp[i].x = f32::min((x as f32 + jx) * dx, ONE_MINUS_EPSILON);
            samp[i].y = f32::min((y as f32 + jy) * dy, ONE_MINUS_EPSILON);
            i += 1;
        }
    }
}

fn Shuffle<T>(samp: &mut [T], count: usize, n_dimensions: usize, rng: &mut RNG) {
    for i in 0..count {
        let other = i + rng.unifrom_u32_range((count - i) as u32) as usize;
        for j in 0..n_dimensions {
            samp.swap(n_dimensions * i + j, n_dimensions * other + j);
        }
    }
}

fn LatinHypercube(samples: &mut [&mut f32], n_samples: usize, n_dim: usize, rng: &mut RNG) {
    let inv_n_samples = 1.0 / n_samples as f32;
    for i in 0..n_samples {
        for j in 0..n_dim {
            let sj = (i as f32 + rng.uniform_f32()) * inv_n_samples;
            *samples[n_dim * i + j] = f32::min(sj, ONE_MINUS_EPSILON);
        }
    }


    for i in 0..n_dim {
        for j in 0..n_samples {
            let other = j + rng.unifrom_u32_range((n_samples - j) as u32) as usize;
            samples.swap(n_dim * j + i, n_dim * other + i);
        }
    }
}


impl SamplerHelper for StratifiedSampler {
    fn samples_per_pixel(&self) -> usize {
        self.samples_per_pixel
    }

    fn change_sample_count(&self, samples_per_pixel: usize) -> Box<Sampler> {
        let diff: i64 = samples_per_pixel as i64 - (self.x_pixel_samples as i64 * self.y_pixel_samples as i64);
        let xs;
        let ys;
        let g = (diff as f64).abs().sqrt().ceil() as usize;
        if diff < 0 {
            xs = self.x_pixel_samples - g;
            ys = self.y_pixel_samples - g;
        } else {
            xs = self.x_pixel_samples + g;
            ys = self.y_pixel_samples + g;
        }
        Box::new(StratifiedSampler::new(xs, ys, self.jitter_samples, self.n_sampled_dimensions))
    }

    fn start_pixel(&mut self, p: Point2i) {
        let rng = &mut self.rng;
        let jitter = self.jitter_samples;
        let xs = self.x_pixel_samples;
        let ys = self.y_pixel_samples;
        let g = xs * ys;
        self.samples_1d.iter_mut().for_each(|x| {
            stratified_sample_1d(x.as_mut_slice(), g, rng, jitter);
            Shuffle(x.as_mut_slice(), g, 1, rng)
        });


        self.samples_2d.iter_mut().for_each(|x| {
            stratified_sample_2d(x.as_mut_slice(), xs, ys, rng, jitter);
            Shuffle(x.as_mut_slice(), g, 1, rng);
        });

        for i in 0..self.samples_1d_array_sizes.len() {
            for j in 0..self.samples_per_pixel {
                let count = self.samples_1d_array_sizes[i];
                stratified_sample_1d(&mut self.sample_array_1d[i][j * count..], count, rng, jitter);
                Shuffle(&mut self.sample_array_1d[i][j * count..], count, 1, rng);
            }
        }

        for i in 0..self.samples_2d_array_sizes.len() {
            for j in 0..self.samples_per_pixel {
                let count = self.samples_2d_array_sizes[i];
                let mut slice: Vec<&mut f32> = self.sample_array_2d[i][j * count..].iter_mut().fold(Vec::new(), |mut acc, p| {
                    acc.push(&mut p.x);
                    acc.push(&mut p.y);
                    acc
                });
                LatinHypercube(&mut slice.as_mut_slice(), count, 2, rng);
            }
        }


        Sampler::start_pixel(self, p);
    }

    fn round_count(&self, n: usize) -> usize {
        unimplemented!()
    }
}



impl_pixel_sampler!(StratifiedSampler);
