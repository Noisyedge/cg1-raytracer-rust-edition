use crate::core::point::Point2i;
use crate::core::point::Point2f;
use crate::rt::samplers::rng::RNG;
use crate::rt::samplers::sampler::SamplerHelper;
use crate::rt::samplers::sampler::SamplerClone;


#[derive(Debug, Clone)]
pub struct RegularSampler {
    current_pixel: Point2i,
    current_pixel_sample_index: usize,
    samples_1d_array_sizes: Vec<usize>,
    samples_2d_array_sizes: Vec<usize>,
    sample_array_1d: Vec<Vec<f32>>,
    sample_array_2d: Vec<Vec<Point2f>>,
    array_1d_offset: usize,
    array_2d_offset: usize,
    samples_per_pixel: usize,
    samples_1d: Vec<Vec<f32>>,
    samples_2d: Vec<Vec<Point2f>>,
    current_1d_dimension: usize,
    current_2d_dimension: usize,
    rng: RNG,
    n_sampled_dimensions: u32,
}

impl SamplerClone for RegularSampler {
    fn clone(&self, _seed: u64) -> Box<Sampler> {
        Box::new(
            Clone::clone(self)
        )
    }
}

impl RegularSampler {
    pub fn new(samples: usize, dimensions: u32) -> RegularSampler {
        RegularSampler {
            current_pixel: Point2i { x: 0, y: 0 },
            current_pixel_sample_index: 0,
            samples_1d_array_sizes: Vec::new(),
            samples_2d_array_sizes: Vec::new(),
            sample_array_1d: Vec::new(),
            sample_array_2d: Vec::new(),
            array_1d_offset: 0,
            array_2d_offset: 0,
            samples_per_pixel: (samples as f64).sqrt().ceil() as usize,
            samples_1d: vec!(vec!(0.0; samples as usize); dimensions as usize),
            samples_2d: vec!(vec!(Point2f { x: 0.0, y: 0.0 }; samples as usize); dimensions as usize),
            current_1d_dimension: 0,
            current_2d_dimension: 0,
            rng: RNG::new(),
            n_sampled_dimensions: dimensions,
        }
    }
}

fn regular_sample_1d(samp: &mut [f32], n_samples: usize) {
    let g = (n_samples as f32).sqrt().ceil() as usize;
    let gridsize = g * g;
    let cellsize_1d = 1.0 / gridsize as f32;
    let offset_1d = cellsize_1d / 2.0;
    for i in 0..n_samples {
        samp[i] = offset_1d + i as f32 * cellsize_1d
    }
}

fn regular_sample_2d(samp: &mut [Point2f], nx: usize, ny: usize) {
    let cellsize_2d = 1.0 / nx as f32;
    let offset_2d = cellsize_2d / 2.0;
    let mut i = 0;
    for y in 0..ny {
        for x in 0..nx {
            samp[i].x = offset_2d + x as f32 * cellsize_2d;
            samp[i].y = offset_2d + y as f32 * cellsize_2d;
            i += 1;
        }
    }
}

impl SamplerHelper for RegularSampler {
    fn samples_per_pixel(&self) -> usize {
        self.samples_per_pixel
    }

    fn change_sample_count(&self, samples_per_pixel: usize) -> Box<Sampler> {
        Box::new(
            RegularSampler::new(samples_per_pixel, self.n_sampled_dimensions)
        )
    }

    fn start_pixel(&mut self, p: Point2i) {
        let g = (self.samples_per_pixel as f64).sqrt().ceil() as usize;
        self.samples_1d.iter_mut().for_each(|x| regular_sample_1d(x.as_mut_slice(), g * g));


        self.samples_2d.iter_mut().for_each(|x| regular_sample_2d(x.as_mut_slice(), g, g));

        for i in 0..self.samples_1d_array_sizes.len() {
            for j in 0..self.samples_per_pixel {
                let count = self.samples_1d_array_sizes[i];
                regular_sample_1d(&mut self.sample_array_1d[i][j * count..], count)
            }
        }

        for i in 0..self.samples_2d_array_sizes.len() {
            for j in 0..self.samples_per_pixel {
                let count = self.samples_2d_array_sizes[i];
                let p = (count as f64).sqrt().ceil() as usize;
                regular_sample_2d(&mut self.sample_array_2d[i][j * count..], p, p)
            }
        }

        SamplerDefault::start_pixel_default(self, p);
    }

    fn round_count(&self, n: usize) -> usize {
        unimplemented!()
    }
}



impl_pixel_sampler!(RegularSampler);





