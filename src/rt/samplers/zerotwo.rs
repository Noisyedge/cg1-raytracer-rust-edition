/*
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNmmmmmmmddddmmMMMMMMMMMMMMMMNmmmNNNNMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNdddhdddddddddddddddmMMMMMMMMMMMMddddddddddddddmmNNMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNhddddddddddddddddddddmNNNNNNNNNNNmdddddddddddddddddddmMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMdhdmddddhhhhhyyssooooo+++++++++++++ooooossyyhhhhdddddddddMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMmhhdhyo+/::-.````````````````````````........```.-:/+oshhdmmMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMNdhho/.`````````````````````````.......````````........```:ohdmMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMmdhy+.`````````````````````````...```````````````````````....`-shNMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMmdhs/``````````````````````````..``````````````````````````````...+hmMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMmdhs:``````````````````````````..``````````````````````````````````.-:ymMMMMMMMMM
MMMMMMMMMMMMMMMMMNdhy/``````````````````````````..`.````````````````````````````````````-:ymMMMMMMMM
MMMMMMMMMMMMMMMNdhy+.``````````````````````````-.`+s-````````````````````````````````````-:ymMMMMMMM
MMMMMMMMMMMMMMmhho.```````````````.```````````-.``syy.````...```...``.````````.+``````````:/hmMMMMMM
MMMMMMMMMMMMNdhy:`````````````````..`````````-.```yyho`               `....``.ss``````````.-+hNMMMMM
MMMMMMMMMMMmhhy.``````````````````.-````````.-``.-yyh:  `````````         ``.ysh```````````-`shMMMMM
MMMMMMMMMNdhdy/```````````````````-:.````...-``.``/s+`..`````````...````    `hsh.``````````.-.hdMMMM
MMMMMMMMNhhddys```````````````````.::......-.`-``..-`-``````````````````..`` hy//.``````````-`/hNMMM
MMMMMMMmhdddddho`````````````````..//:.....: --.````..`````````````````````- +:.` ..````````-`.ydMMM
MMMMMMmhdddddddhs.``````````````...://:..---`.``````.-``````````````````````.`...` `.```````-``:hmMM
MMMMMmhdddddddmdhy:```````````.....-//+:-.-.```````...`````````` `````````````````.``-.`````:`.`shMM
MMMMmhdddddddddmmdho.```````........:+:.../````.``.` .`````````` `````` ````.```````..-`````:...:hNM
MMMmhddddddddddddmdhy/.```.........../..::/````.`.`  -`:`````.``````.``````.:```.`````..````:....hdM
MMNhdmmmmdddddddddmmdhs:............:../:.:```....   -`:.````-``````:``````.`-``.``````````.:....ohN
MNdhmmmmmmddddddddddmddho-..........:.::.-:```-.- `.`...-````/`````..-`````. -``.``````````.:....:hm
Mdhdmmmmmmmmmddddddddmmddho-........:./-.--.``--   `../.-````+.````- -````-```-``:``````.`..:.....dm
Nhdmmmmmmmmmmmmmddddddmmmddho-......--/..--..`:-///..`/---.``/-````-`--``.-`` ..`:`````.....:.....hd
mhdmmmmmmmmmmmmmmmddddddmmmddho:...../:..--..`-`-:/osoo:..-.`.--``.-.:/`.:     -`-`````-`-..:....-hm
mddmmmmmmmmmmmmmmmmmmdddmmmmmddhs:-.-:-..-/...- ```  `.-. `.```-``.`.+:-o///:-..--````..`-..:...-ydM
Mddmmmmmmmmmmmmmmmmmmmmmmmmmmmmddhs/--/...--..-                 ``  ./::+//+++/-:.```.-  -.:..-+hdmM
MMddmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmddy+/-...:..:                             ````-````-` ...:-/ydmmMM
MMMmdmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmddho...:..-`           .``                 `-```.-`-...oydmmNMMM
MMMMNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmdh:..-...-           `                   -....:--...-dmNNMMMMM
MMMMMMNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmy..-...-           .                   -...-/.....:mMMMMMMMM
MMMMMMMMNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm+..-..:                              `-.../....../MMMMMMMMM
MMMMMMMMMMNmmmmmmmmmmmmmdddhhdmNNNNNmmmmmd:..../-`     .:````````````:-       :...--.....:+MMMMMMMMM
MMMMMMMMMMMMNmmmmmmmddhhhhhhhhhdNNNNNmmmmmd-...-//-`    /`        ```+:     `-/...:....../oMMMMMMMMM
MMMMMMMMMMMMMMNmmmmdhhhhhhhhhhhhdNNNNNmmmmmy-.../+-:-`   ``     `````     `---:..--.....-/sNMMMMMMMM
MMMMMMMMMMMMMMMMNmddhhhhhhhhhhhhhhNNNNNmmmmm+-../+---:-.               `.---.:...:....../:shdNMMMMMM
MMMMMMMMMMMMMMMMMmddhhhhhhhhhhhhhhhNNNNNmmmmd---/+.------.         ``.:---...:...:....--+:hhhhdNMMMM
MMMMMMMMMMMMMMMMNdddddhhhhhhhhhhhhhdNNNNNmmmmo---o`..------.``......--.......:..:-..---//+hhhhhhmMMM
MMMMMMMMMMMMMMMMmddddddhhhhhhhhhhhhhmNNNNNmmmd---yy.`.......--------.........:../.-----+/ohhhhhhhdMM
MMMMMMMMMMMMMMMMmdddddddhhhhhhhhhhhhdNNNNNNNNN+--omh-```....................:.../-----///yddddddddNM
 */

use crate::core::point::Point2i;
use crate::core::point::Point2f;
use crate::rt::samplers::rng::RNG;
use crate::rt::samplers::sampler::SamplerHelper;
use crate::rt::samplers::sampler::SamplerClone;
use crate::rt::samplers::rng::ONE_MINUS_EPSILON;


use hexf::*;
use std::ops::BitAnd;
use std::ops::Sub;


#[derive(Debug, Clone)]
pub struct ZeroTwoSampler {
    current_pixel: Point2i,
    current_pixel_sample_index: usize,
    samples_1d_array_sizes: Vec<usize>,
    samples_2d_array_sizes: Vec<usize>,
    sample_array_1d: Vec<Vec<f32>>,
    sample_array_2d: Vec<Vec<Point2f>>,
    array_1d_offset: usize,
    array_2d_offset: usize,
    samples_per_pixel: usize,
    samples_1d: Vec<Vec<f32>>,
    samples_2d: Vec<Vec<Point2f>>,
    current_1d_dimension: usize,
    current_2d_dimension: usize,
    rng: RNG,
    n_sampled_dimensions: u32,
}

impl SamplerClone for ZeroTwoSampler {
    fn clone(&self, _seed: u64) -> Box<Sampler> {
        Box::new(
            Clone::clone(self)
        )
    }
}

impl ZeroTwoSampler {
    pub fn new(samples: usize, dimensions: u32) -> ZeroTwoSampler {
        ZeroTwoSampler {
            current_pixel: Point2i { x: 0, y: 0 },
            current_pixel_sample_index: 0,
            samples_1d_array_sizes: Vec::new(),
            samples_2d_array_sizes: Vec::new(),
            sample_array_1d: Vec::new(),
            sample_array_2d: Vec::new(),
            array_1d_offset: 0,
            array_2d_offset: 0,
            samples_per_pixel: (samples as f64).sqrt().ceil() as usize,
            samples_1d: vec!(vec!(0.0; samples as usize); dimensions as usize),
            samples_2d: vec!(vec!(Point2f { x: 0.0, y: 0.0 }; samples as usize); dimensions as usize),
            current_1d_dimension: 0,
            current_2d_dimension: 0,
            rng: RNG::new(),
            n_sampled_dimensions: dimensions,
        }
    }
}

fn round_up_pow_2_i32(s: i32) -> i32 {
    let mut v = s;
    v -= 1;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v + 1
}

fn round_up_pow_2_i64(s: i64) -> i64 {
    let mut v = s;
    v-=1;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v |= v >> 32;
    v + 1
}

fn is_power_of_2<T>(v: T) -> bool
where
    T: PartialEq<usize>,
    T: Sub<usize>,
    T: BitAnd<<T as Sub<usize>>::Output>,
    <T as BitAnd<<T as Sub<usize>>::Output>>::Output: PartialEq<usize>,
    T: Copy
{
    (v != 0) && ((v & (v - 1)) == 0)
}

const CSOBOL: [[u32; 32]; 2] = [
    [
        0x80000000, 0x40000000, 0x20000000, 0x10000000, 0x8000000, 0x4000000,
        0x2000000, 0x1000000, 0x800000, 0x400000, 0x200000, 0x100000, 0x80000,
        0x40000, 0x20000, 0x10000, 0x8000, 0x4000, 0x2000, 0x1000, 0x800,
        0x400, 0x200, 0x100, 0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1
    ],
    [
        0x80000000, 0xc0000000, 0xa0000000, 0xf0000000, 0x88000000, 0xcc000000,
        0xaa000000, 0xff000000, 0x80800000, 0xc0c00000, 0xa0a00000, 0xf0f00000,
        0x88880000, 0xcccc0000, 0xaaaa0000, 0xffff0000, 0x80008000, 0xc000c000,
        0xa000a000, 0xf000f000, 0x88008800, 0xcc00cc00, 0xaa00aa00, 0xff00ff00,
        0x80808080, 0xc0c0c0c0, 0xa0a0a0a0, 0xf0f0f0f0, 0x88888888, 0xcccccccc,
        0xaaaaaaaa, 0xffffffff
    ]
];

fn sobol_2d(n_samples_per_pixel_sample: usize, n_pixel_samples: usize, samples: &mut [Point2f], rng: &mut RNG) {
    let scramble = Point2i { x: rng.uniform_u32() as i32, y: rng.uniform_u32() as i32 };

    gray_code_sample_2d(&CSOBOL[0], &CSOBOL[1], (n_samples_per_pixel_sample * n_pixel_samples) as u32, scramble, samples);

    for i in 0..n_pixel_samples {
        shuffle(&mut samples[i * n_samples_per_pixel_sample..], n_samples_per_pixel_sample, 1, rng);
        shuffle(samples, n_pixel_samples, n_samples_per_pixel_sample, rng);
    }
}

const CVAN_DER_CORPUT: [u32; 32] = [
    0b10000000000000000000000000000000,
    0b1000000000000000000000000000000,
    0b100000000000000000000000000000,
    0b10000000000000000000000000000,
    0b10000,
    0b100000,
    0b1000000,
    0b10000000,
    0b100000000,
    0b1000000000,
    0b10000000000,
    0b100000000000,
    0b1000000000000,
    0b10000000000000,
    0b100000000000000,
    0b1000000000000000,
    0b10000000000000000,
    0b100000000000000000,
    0b1000000000000000000,
    0b10000000000000000000,
    0b100000000000000000000,
    0b1000000000000000000000,
    0b10000000000000000000000,
    0b100000000000000000000000,
    0b1000000000000000000000000,
    0b10000000000000000000000000,
    0b100000000000000000000000000,
    0b1000000000000000000000000000,
    0b10000000000000000000000000000,
    0b100000000000000000000000000000,
    0b1000000000000000000000000000000,
    0b10000000000000000000000000000000,
];

fn van_der_corput(n_samples_per_pixel_sample: usize, n_pixel_samples: usize, samples: &mut [f32], rng: &mut RNG) {
    let scramble = rng.uniform_u32();
    let total_samples = n_samples_per_pixel_sample * n_pixel_samples;
    gray_code_sample_1d(&CVAN_DER_CORPUT, total_samples as u32, scramble, samples);
    for i in 0..n_pixel_samples {
        shuffle(&mut samples[i * n_samples_per_pixel_sample..], n_samples_per_pixel_sample, 1, rng);
        shuffle(samples, n_pixel_samples, n_samples_per_pixel_sample, rng);
    }
}

fn gray_code_sample_1d(C: &[u32], n: u32, scramble: u32, p: &mut [f32]) {
    let mut v = scramble;
    for i in 0..n {
        p[i as usize] = v as f32 * hexf32!("0x1.0p-32");
        v ^= C[count_trailing_zeros(i + 1) as usize];
    }
}

fn gray_code_sample_2d(c0: &[u32], c1: &[u32], n: u32, scramble: Point2i, p: &mut [Point2f]) {
    let mut v = [scramble.x as u32, scramble.y as u32];
    for i in 0..n {
        p[i as usize].x = f32::min(v[0] as f32 * 2.328_306_436_538_696_3e-10f32, ONE_MINUS_EPSILON);
        p[i as usize].y = f32::min(v[1] as f32 * 2.328_306_436_538_696_3e-10f32, ONE_MINUS_EPSILON);
        v[0] ^= c0[count_trailing_zeros(i + 1) as usize];
        v[1] ^= c1[count_trailing_zeros(i + 1) as usize];
    }
}

fn count_trailing_zeros(v: u32) -> u32 {
    v.trailing_zeros()
}

fn shuffle<T>(samp: &mut [T], count: usize, n_dimensions: usize, rng: &mut RNG) {
    for i in 0..count {
        let other = i + rng.unifrom_u32_range((count - 1) as u32) as usize;
        for j in 0..n_dimensions {
            samp.swap(n_dimensions * i + j, n_dimensions * other + j);
        }
    }
}

impl SamplerHelper for ZeroTwoSampler {
    fn samples_per_pixel(&self) -> usize {
        self.samples_per_pixel
    }

    fn change_sample_count(&self, samples_per_pixel: usize) -> Box<Sampler> {
        Box::new(
            ZeroTwoSampler::new(samples_per_pixel, self.n_sampled_dimensions)
        )
    }

    fn start_pixel(&mut self, p: Point2i) {
        let samples = self.samples_per_pixel;
        let rng = &mut self.rng;
        self.samples_1d.iter_mut().for_each(|x| van_der_corput(1, samples, x.as_mut_slice(), rng));
        self.samples_2d.iter_mut().for_each(|x| sobol_2d(1, samples, x.as_mut_slice(), rng));

        for i in 0..self.samples_1d_array_sizes.len(){
            van_der_corput(self.samples_1d_array_sizes[i], samples,
            self.sample_array_1d[i].as_mut_slice(), &mut self.rng);
        }

        for i in 0..self.samples_2d_array_sizes.len(){
            sobol_2d(self.samples_2d_array_sizes[i], samples,
            self.sample_array_2d[i].as_mut_slice(), &mut self.rng);
        }

        Sampler::start_pixel(self, p)


    }

    fn round_count(&self, n: usize) -> usize {
        round_up_pow_2_i64(n as i64) as usize
    }
}



impl_pixel_sampler!(ZeroTwoSampler);




