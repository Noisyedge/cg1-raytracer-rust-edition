use crate::rt::samplers::sampler::*;
use crate::core::point::Point2f;

#[macro_export]
macro_rules! impl_pixel_sampler {
    ($struct:ty) => {

        use crate::rt::samplers::sampler::*;

        impl SamplerAquire for $struct{
            fn get_1d(&mut self) -> f32{
                if self.current_1d_dimension < self.samples_1d.len(){
                    let r = self.samples_1d[self.current_1d_dimension][self.current_pixel_sample_index];
                    self.current_1d_dimension += 1;
                    r
                }
                else{
                    self.rng.uniform_f32()
                }
            }

            fn get_2d(&mut self) -> Point2f{
                if self.current_2d_dimension < self.samples_2d.len(){
                    let r = self.samples_2d[self.current_2d_dimension][self.current_pixel_sample_index];
                    self.current_2d_dimension += 1;
                    r
                }
                else{
                    Point2f{
                        x: self.rng.uniform_f32(),
                        y: self.rng.uniform_f32()
                    }
                }
            }

            fn start_next_sample(&mut self) -> bool{
                self.current_1d_dimension = 0;
                self.current_2d_dimension = 0;
                SamplerDefault::start_next_sample_default(self)

            }

            fn set_sample_number(&mut self, n: usize) -> bool{
                self.current_1d_dimension = 0;
                self.current_2d_dimension = 0;
                SamplerDefault::set_sample_number_default(self, n)
            }
        }

        impl_sampler_default!($struct);

    }
}