use crate::core::point::Point2i;
use crate::core::point::Point2f;
use crate::rt::cameras::camera::CameraSample;

pub trait SamplerAquire {
    fn get_1d(&mut self) -> f32;

    fn get_2d(&mut self) -> Point2f;

    fn start_next_sample(&mut self) -> bool;

    fn set_sample_number(&mut self, n: usize) -> bool;
}

pub trait SamplerHelper {
    fn samples_per_pixel(&self) -> usize;

    fn change_sample_count(&self, samples_per_pixel: usize) -> Box<Sampler>;

    fn start_pixel(&mut self, p: Point2i);

    fn round_count(&self, n: usize) -> usize {
        n
    }
}

pub trait SamplerClone {
    fn clone(&self, seed: u64) -> Box<Sampler>;
}

pub trait SamplerDefault {
    fn start_pixel_default(&mut self, p: Point2i);

    fn request_1d_array(&mut self, n: usize);

    fn request_2d_array(&mut self, n: usize);


    fn get_1d_array(&mut self, n: usize) -> Option<&mut f32>;

    fn get_2d_array(&mut self, n: usize) -> Option<&mut Point2f>;

    fn start_next_sample_default(&mut self) -> bool;


    fn set_sample_number_default(&mut self, n: usize) -> bool;


}

pub trait Sampler: SamplerHelper + SamplerClone + SamplerAquire + SamplerDefault {
    fn get_camera_sample(&mut self, pRaster: Point2i) -> CameraSample {
        let p = self.get_2d();
        CameraSample {
            p: Point2f { x: pRaster.x as f32 + p.x, y: pRaster.y as f32 + p.y },
            time: self.get_1d(),
            lens_uv: self.get_2d()
        }
    }
}

#[macro_export]
macro_rules! impl_sampler_default {

    ($struct:ty) => {

        use crate::rt::samplers::sampler::Sampler;

        impl Sampler for $struct{}

        impl SamplerDefault for $struct{
            fn start_pixel_default(&mut self, p: Point2i){
                self.current_pixel = p;
                self.current_pixel_sample_index = 0;
                self.array_1d_offset = 0;
                self.array_2d_offset = 0;
            }

            fn request_1d_array(&mut self, n: usize){
                self.samples_1d_array_sizes.push(n);
                self.sample_array_1d.push(vec!(0.0; n * self.samples_per_pixel));
            }

            fn request_2d_array(&mut self, n: usize){
                self.samples_2d_array_sizes.push(n);
                self.sample_array_2d.push(vec!(Point2f{x: 0.0, y: 0.0}; n * self.samples_per_pixel));
            }



            fn get_1d_array(&mut self, n: usize) -> Option<&mut f32>{
                if self.array_1d_offset == self.sample_array_1d.len(){
                    None
                }
                else{
                    let r = &mut self.sample_array_1d[self.array_1d_offset][self.current_pixel_sample_index * n];
                    self.array_1d_offset+=1;
                    Some(r)
                }
            }

            fn get_2d_array(&mut self, n: usize) -> Option<&mut Point2f>{
                if self.array_2d_offset == self.sample_array_2d.len(){
                    None
                }
                else{
                    let r = &mut self.sample_array_2d[self.array_2d_offset][self.current_pixel_sample_index * n];
                    self.array_2d_offset+=1;
                    Some(r)
                }
            }

            fn start_next_sample_default(&mut self) -> bool{
                self.array_1d_offset = 0;
                self.array_2d_offset = 0;
                self.current_pixel_sample_index += 1;
                self.current_pixel_sample_index < self.samples_per_pixel
            }



            fn set_sample_number_default(&mut self, n: usize) -> bool{
                self.array_1d_offset = 0;
                self.array_2d_offset = 0;
                self.current_pixel_sample_index = n;
                self.current_pixel_sample_index < self.samples_per_pixel
            }
        }
    }
}

