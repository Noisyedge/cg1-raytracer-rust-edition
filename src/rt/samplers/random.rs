use crate::core::point::Point2i;
use crate::core::point::Point2f;
use crate::rt::samplers::rng::RNG;

#[derive(Debug, Clone)]
pub struct RandomSampler {
    current_pixel: Point2i,
    current_pixel_sample_index: usize,
    samples_1d_array_sizes: Vec<usize>,
    samples_2d_array_sizes: Vec<usize>,
    sample_array_1d: Vec<Vec<f32>>,
    sample_array_2d: Vec<Vec<Point2f>>,
    array_1d_offset: usize,
    array_2d_offset: usize,
    samples_per_pixel: usize,
    samples_1d: Vec<Vec<f32>>,
    samples_2d: Vec<Vec<Point2f>>,
    current_1d_dimension: usize,
    current_2d_dimension: usize,
    rng: RNG,
    n_sampled_dimensions: u32,
}

impl SamplerClone for RandomSampler {
    fn clone(&self, seed: u64) -> Box<Sampler> {
        let mut ss = Box::new(
            Clone::clone(self)
        );
        ss.rng.set_sequence(seed);
        ss
    }
}

impl RandomSampler {
    pub fn new(samples: usize, dimensions: u32) -> RandomSampler {
        RandomSampler {
            current_pixel: Point2i { x: 0, y: 0 },
            current_pixel_sample_index: 0,
            samples_1d_array_sizes: Vec::new(),
            samples_2d_array_sizes: Vec::new(),
            sample_array_1d: Vec::new(),
            sample_array_2d: Vec::new(),
            array_1d_offset: 0,
            array_2d_offset: 0,
            samples_per_pixel: (samples as f64).sqrt().ceil() as usize,
            samples_1d: vec!(vec!(0.0; samples as usize); dimensions as usize),
            samples_2d: vec!(vec!(Point2f { x: 0.0, y: 0.0 }; samples as usize); dimensions as usize),
            current_1d_dimension: 0,
            current_2d_dimension: 0,
            rng: RNG::new(),
            n_sampled_dimensions: dimensions,
        }
    }
}

fn random_sample_1d(samp: &mut [f32], n_samples: usize, rng: &mut RNG) {
    for i in samp.iter_mut().take(n_samples) {
        *i = rng.uniform_f32();
    }
}

fn random_sample_2d(samp: &mut [Point2f], nx: usize, ny: usize, rng: &mut RNG) {
    for i in samp.iter_mut().take(nx*ny) {
        i.x = rng.uniform_f32();
        i.y = rng.uniform_f32();
    }
}


impl SamplerHelper for RandomSampler {
    fn samples_per_pixel(&self) -> usize {
        self.samples_per_pixel
    }

    fn change_sample_count(&self, samples_per_pixel: usize) -> Box<Sampler> {
        Box::new(
            RandomSampler::new(samples_per_pixel, self.n_sampled_dimensions)
        )
    }

    fn start_pixel(&mut self, p: Point2i) {
        let rng = &mut self.rng;
        let g = (self.samples_per_pixel as f64).sqrt().ceil() as usize;
        self.samples_1d.iter_mut().for_each(|x| random_sample_1d(x.as_mut_slice(), g * g, rng));


        self.samples_2d.iter_mut().for_each(|x| random_sample_2d(x.as_mut_slice(), g, g, rng));

        for i in 0..self.samples_1d_array_sizes.len() {
            for j in 0..self.samples_per_pixel {
                let count = self.samples_1d_array_sizes[i];
                random_sample_1d(&mut self.sample_array_1d[i][j * count..], count, &mut self.rng)
            }
        }

        for i in 0..self.samples_2d_array_sizes.len() {
            for j in 0..self.samples_per_pixel {
                let count = self.samples_2d_array_sizes[i];
                let p = (count as f64).sqrt().ceil() as usize;
                random_sample_2d(&mut self.sample_array_2d[i][j * count..], p, p, &mut self.rng)
            }
        }

        Sampler::start_pixel(self, p);
    }

    fn round_count(&self, n: usize) -> usize {
        unimplemented!()
    }
}



impl_pixel_sampler!(RandomSampler);


