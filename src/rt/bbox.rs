use crate::core::point::Point;
use crate::core::vector::Vector;
use crate::rt::ray::Ray;
use std::mem::swap;
use num_traits::float::Float;

#[derive(Clone, Copy)]
pub struct BBox {
    pub min: Point,
    pub max: Point,
    middle: Point
}



impl BBox {
    pub fn new(min: Point, max: Point) -> BBox {
        BBox {
            min,
            max,
            middle: max + (max - min) * 0.5
        }
    }

    pub fn empty() -> BBox {
        BBox::new(
            Point::rep(f32::infinity()),
            Point::rep(f32::neg_infinity())
        )
    }

    pub fn full() -> BBox {
        BBox::new(
            Point::rep(f32::neg_infinity()),
            Point::rep(f32::infinity())
        )
    }

    pub fn extend(&mut self, point: Point) {
        self.min = Point::new(
            f32::min(self.min.x, point.x),
            f32::min(self.min.y, point.y),
            f32::min(self.min.z, point.z)
        );

        self.max = Point::new(
            f32::max(self.max.x, point.x),
            f32::max(self.max.y, point.y),
            f32::max(self.max.z, point.z)
        );

        self.middle = self.min + self.diagonal() * 0.5;
    }

    pub fn union(&mut self, bbox: BBox) {
        self.min = Point::new(
            f32::min(self.min.x, bbox.min.x),
            f32::min(self.min.y, bbox.min.y),
            f32::min(self.min.z, bbox.min.z)
        );

        self.max = Point::new(
            f32::max(self.max.x, bbox.max.x),
            f32::max(self.max.y, bbox.max.y),
            f32::max(self.max.z, bbox.max.z)
        );

        self.middle = self.min + self.diagonal() * 0.5;
    }

    pub fn extend_return(&self, point: Point) -> BBox {
        BBox::new(
            Point::new(
                f32::min(self.min.x, point.x),
                f32::min(self.min.y, point.y),
                f32::min(self.min.z, point.z)
            ),
            Point::new(
                f32::max(self.max.x, point.x),
                f32::max(self.max.y, point.y),
                f32::max(self.max.z, point.z)
            )
        )
    }

    pub fn union_return(&self, bbox: BBox) -> BBox {
        BBox::new(
            Point::new(
                f32::min(self.min.x, bbox.min.x),
                f32::min(self.min.y, bbox.min.y),
                f32::min(self.min.z, bbox.min.z)
            ),
            Point::new(
                f32::max(self.max.x, bbox.max.x),
                f32::max(self.max.y, bbox.max.y),
                f32::max(self.max.z, bbox.max.z)
            )
        )
    }

    pub fn middle(&self) -> Point {
        self.middle
    }

    pub fn intersect(&self, ray: Ray) -> (f32, f32) {


//Simple Slab Algorithm
        let mut tmin;
        let mut tmax;

        let invdir = Vector::new(1.0 / ray.d.x, 1.0 / ray.d.y, 1.0 / ray.d.z);


        let mut tminx = (self.min.x - ray.o.x) * invdir.x;
        let mut tmaxx = (self.max.x - ray.o.x) * invdir.x;

        if tminx > tmaxx {
            swap(&mut tminx, &mut tmaxx);
        }


        tmin = tminx;
        tmax = tmaxx;


        let mut tminy = (self.min.y - ray.o.y) * invdir.y;
        let mut tmaxy = (self.max.y - ray.o.y) * invdir.y;

        if tminy > tmaxy {
            swap(&mut tminy, &mut tmaxy);
        }


        if tmin > tmaxy {
            return (tmin, tmaxy);
        }
        if tminy > tmax { return (tminy, tmax); }


        if tminy > tmin {
            tmin = tminy;
        }
        if tmaxy < tmax {
            tmax = tmaxy;
        }

        let mut tminz = (self.min.z - ray.o.z) * invdir.z;
        let mut tmaxz = (self.max.z - ray.o.z) * invdir.z;

        if tminz > tmaxz {
            swap(&mut tminz, &mut tmaxz);
        }


        if tmin > tmaxz { return (tmin, tmaxz); }
        if tminz > tmax { return (tminz, tmax); }

        if tminz > tmin {
            tmin = tminz;
        }


        if tmaxz < tmax {
            tmax = tmaxz;
        }


        (tmin, tmax)
    }


    pub fn is_unbound(&self) -> bool {
        ((self.min.x == f32::neg_infinity() || self.max.x == f32::infinity())
            || (self.min.y == f32::neg_infinity() || self.max.y == f32::infinity())
            || (self.min.z == f32::neg_infinity() || self.max.z == f32::infinity()))
    }
    pub fn is_unbound_x(&self) -> bool {
        (self.min.x == f32::neg_infinity() || self.max.x == f32::infinity())
    }
    pub fn is_unbound_y(&self) -> bool {
        (self.min.y == f32::neg_infinity() || self.max.y == f32::infinity())
    }
    pub fn is_unbound_z(&self) -> bool {
        (self.min.z == f32::neg_infinity() || self.max.z == f32::infinity())
    }
    pub fn surface_area(&self) -> f32 {
        let diag = self.diagonal();
        2.0 * (diag.x * diag.y + diag.x * diag.z + diag.y * diag.z)
    }


    pub fn diagonal(&self) -> Vector {
        self.max - self.min
    }
}