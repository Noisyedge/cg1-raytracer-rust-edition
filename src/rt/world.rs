use crate::rt::primitive::Primitive;
use crate::rt::lights::light::Light;

pub struct World<'a>{
    pub scene: &'a Primitive,
    pub light: Vec<&'a Light>

}

impl<'a> World<'a>{
    pub fn new(scene: &'a Primitive) -> World<'a>{
        World{
            scene,
            light: Vec::new()
        }
    }
}