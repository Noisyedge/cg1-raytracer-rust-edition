use crate::core::point::Point;
use crate::rt::mediums::medium::Medium;
use crate::rt::cameras::camera::Camera;
use crate::rt::ray::Ray;
use crate::core::vector::Vector;
use crate::core::vector::cross;
use std::rc::Rc;

#[derive(Clone)]
pub struct OrthographicCamera {
    center: Point,
    forward: Vector,
    up: Vector,
    right: Vector,
    scale_x: f32,
    scale_y: f32,
    medium: Option<Rc<Medium>>
}

impl OrthographicCamera {
    pub fn new(center: Point, forward: Vector, up: Vector, scale_x: f32, scale_y: f32, medium: Option<Rc<Medium>>) -> OrthographicCamera {
        OrthographicCamera {
            center,
            forward: forward.normalize(),
            up: up.normalize(),
            right: cross(forward, up).normalize(),
            scale_x,
            scale_y,
            medium
        }
    }
}


impl Camera for OrthographicCamera {
    fn get_primary_ray_no_blur(&self, x: f32, y: f32) -> Ray {
        let orig = self.center + (x * self.scale_x / 2.0 * self.right) + (y * self.scale_y / 2.0 * self.up);
        Ray::new_medium(orig, self.forward, 0.0, self.medium.clone())
    }

    fn get_medium(&self) -> Option<Rc<Medium>> {
        self.medium.clone()
    }
}










