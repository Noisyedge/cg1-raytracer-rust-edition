use crate::core::point::Point;
use crate::rt::mediums::medium::Medium;
use crate::rt::cameras::camera::Camera;
use crate::rt::ray::Ray;
use crate::core::vector::Vector;
use crate::core::vector::cross;
use crate::rt::solids::disc::Disc;
use std::rc::Rc;
use crate::rt::solids::solid::Solid;

#[derive(Clone)]
pub struct DOFPerspectiveCamera{
    center: Point,
    forward: Vector,
    up: Vector,
    right: Vector,
    up_calc: Vector,
    vertical_opening_angle: f32,
    horizontal_opening_angle: f32,
    focal_distance: f32,
    aperture_radius: f32,
    horizontal: Vector,
    vertical: Vector,
    aperture_disc: Disc,
    medium: Option<Rc<Medium>>
}

impl DOFPerspectiveCamera{
    pub fn new(center: Point, forward: Vector, up: Vector, vertical_opening_angle: f32, horizontal_opening_angle: f32, focal_distance: f32, aperture_radius: f32, medium: Option<Rc<Medium>>) -> DOFPerspectiveCamera {
        let right = cross(forward, up).normalize();
        let up_calc = cross(right, forward).normalize();
        DOFPerspectiveCamera{
            center,
            forward: forward.normalize(),
            up: up.normalize(),
            right,
            up_calc,
            vertical_opening_angle,
            horizontal_opening_angle,
            focal_distance,
            aperture_radius,
            horizontal: right * (horizontal_opening_angle / 2.0).tan(),
            vertical: up_calc * (vertical_opening_angle / 2.0).tan(),
            aperture_disc: Disc::new(center, forward, aperture_radius, None,  None),
            medium,

        }
    }



}



impl Camera for DOFPerspectiveCamera {
    fn get_primary_ray_no_blur(&self, x: f32, y: f32) -> Ray {
        let h = self.center + self.focal_distance * (self.forward + x * self.horizontal + y * self.vertical);
        let od;
        od = self.aperture_disc.sample().point;
        let d = h - od;
        Ray::new_medium(od, d.normalize(), 0.0, self.medium.clone())
    }

    fn get_primary_ray_no_time(&self, x: f32, y: f32, u: f32, v: f32) -> Ray {
        let h = self.center + self.focal_distance * (self.forward + x * self.horizontal + y * self.vertical);
        let od;
        od = self.aperture_disc.sample_range(u, v).point;
        let d = h - od;
        Ray::new_medium(od, d.normalize(), 0.0, self.medium.clone())
    }


    fn get_medium(&self) -> Option<Rc<Medium>> {
        self.medium.clone()
    }
}










