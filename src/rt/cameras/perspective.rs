use crate::core::point::Point;
use crate::rt::mediums::medium::Medium;
use crate::rt::cameras::camera::Camera;
use crate::rt::ray::Ray;
use crate::core::vector::Vector;
use crate::core::vector::cross;
use std::rc::Rc;

#[derive(Clone)]
pub struct PerspectiveCamera {
    center: Point,
    forward: Vector,
    up: Vector,
    right: Vector,
    up_calc: Vector,
    vertical_opening_angle: f32,
    horizontal_opening_angle: f32,
    scale_x: f32,
    scale_y: f32,
    medium: Option<Rc<Medium>>
}

impl PerspectiveCamera {
    pub fn new(center: Point, forward: Vector, up: Vector, vertical_opening_angle: f32, horizontal_opening_angle: f32, medium: Option<Rc<Medium>>) -> PerspectiveCamera {
        let right = cross(forward, up).normalize();
        let up_calc = cross(right, forward).normalize();
        PerspectiveCamera {
            center,
            forward: forward.normalize(),
            up: up.normalize(),
            right,
            up_calc,
            vertical_opening_angle,
            horizontal_opening_angle,
            scale_x: (horizontal_opening_angle / 2.0).tan(),
            scale_y: (vertical_opening_angle / 2.0).tan(),
            medium
        }
    }

    pub fn set_scale_x(&mut self, scale_x: f32) {
        self.scale_x = scale_x;
    }

    pub fn set_scale_y(&mut self, scale_y: f32) {
        self.scale_y = scale_y;
    }
}


impl Camera for PerspectiveCamera {
    fn get_primary_ray_no_blur(&self, x: f32, y: f32) -> Ray {
        let dir = (self.forward + (x * self.scale_x * self.right) + (y * self.scale_y * self.up_calc)).normalize();
        Ray::new_medium(self.center, dir, 0.0, self.medium.clone())
    }

    fn get_medium(&self) -> Option<Rc<Medium>> {
        self.medium.clone()
    }
}










