use crate::core::point::Point2f;
use crate::rt::ray::Ray;
use crate::rt::mediums::medium::Medium;
use std::rc::Rc;

pub trait Camera {
    fn get_primary_ray_no_blur(&self, x: f32, y: f32) -> Ray;

    fn get_primary_ray(&self, x: f32, y: f32, _u: f32, _v: f32, _t: f32) -> Ray {
        self.get_primary_ray_no_blur(x, y)
    }

    fn get_primary_ray_no_time(&self, x: f32, y: f32, _u: f32, _v: f32) -> Ray {
        self.get_primary_ray_no_blur(x, y)
    }

    fn get_medium(&self) -> Option<Rc<Medium>>;
}

#[derive(Copy, Clone, Debug, Default)]
pub struct CameraSample {
    pub p: Point2f,
    pub time: f32,
    pub lens_uv: Point2f
}
