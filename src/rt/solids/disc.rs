use crate::core::point::Point;
use crate::core::vector::Vector;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::materials::material::Material;
use crate::rt::mediums::medium::Medium;
use crate::rt::solids::solid::Sample;
use crate::rt::bbox::BBox;
use crate::core::vector::cross;
use crate::rt::solids::solid::Solid;
use crate::rt::ray::Ray;
use crate::rt::intersection::Intersection;
use crate::core::vector::orth;
use rand::thread_rng;
use std::f32::consts::PI;
use rand::Rng;
use crate::rt::solids::infinite_plane::InfinitePlane;

#[derive(Clone)]
pub struct Disc{
    center: Point,
    normal: Vector,
    radius: f32,
    radius_squared: f32,
    bbox: BBox,
    material: Option<Rc< Material>>,
    tex_mapper: Option<Rc< CoordMapper>>,
    id: u32,
    inside: Option<Rc< Medium>>,
    outside: Option<Rc< Medium>>,
    is_medium: bool
}

impl Disc{
    pub fn new(center: Point, normal: Vector, radius: f32, tex_mapper: Option<Rc<CoordMapper>>, material: Option<Rc<Material>>) -> Disc {
        let v1;
        //get a Vector that is definitely not parralell to the normal
        if normal.x == 0.0 {
            v1 = Vector::new(normal.x + 1.0, normal.y, normal.z);
        } else if normal.y == 0.0 {
            v1 = Vector::new(normal.x, normal.y + 1.0, normal.z);
        } else if normal.z == 0.0 {
            v1 = Vector::new(normal.x, normal.y, normal.z + 1.0);
        } else {
            v1 = Vector::new(-normal.x, normal.y, normal.z);
        }
        let o1 = cross(normal, v1).normalize();
        let o2 = cross(o1, normal).normalize();
        let p1 = center + (o1 * radius);
        let p2 = center - (o1 * radius);
        let p3 = center + (o2 * radius);
        let p4 = center - (o2 * radius);
        let bbox = BBox::new(
            Point::new(
                f32::min(f32::min(p1.x, p2.x), f32::min(p3.x, p4.x)),
                f32::min(f32::min(p1.y, p2.y), f32::min(p3.y, p4.y)),
                f32::min(f32::min(p1.z, p2.z), f32::min(p3.z, p4.z))
            ),
            Point::new(
                f32::max(f32::max(p1.x, p2.x), f32::max(p3.x, p4.x)),
                f32::max(f32::max(p1.y, p2.y), f32::max(p3.y, p4.y)),
                f32::max(f32::max(p1.z, p2.z), f32::max(p3.z, p4.z))
            )
        );
        Disc {
            center,
            normal,
            radius,
            radius_squared: radius * radius,
            bbox,
            material,
            tex_mapper,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false
        }
    }
}

impl_primitive_fields!(Disc);

impl Primitive for Disc{
    fn get_bounds(&self) -> BBox {
        self.bbox
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        let plane = InfinitePlane::new(self.center, self.normal, None, None);
        let intersection_p = plane.intersect(ray.clone(), previous_best_distance);


        if !intersection_p.failure {
            if intersection_p.distance <= 0.0 || intersection_p.distance > previous_best_distance{
            return Intersection::failure();}
            let on_plane = ray.o + ray.d * intersection_p.distance;
            let v = on_plane - self.center;
            if v.lensqr() <= self.radius_squared {
                return Intersection::new(intersection_p.distance, ray, self, self.normal.normalize(), intersection_p.local());
            }
            return Intersection::failure();

        }

        Intersection::failure()
    }
}

impl Solid for Disc{
    fn sample(&self) -> Sample {
        let mut rng = thread_rng();
        let theta =  rng.gen_range(0.0, 2.0 * PI);
        let y: f32 = rng.gen();
        let r = y.sqrt() * self.radius;
        let in_disc1 = orth(&self.normal).normalize();
        let in_disc2 = cross(self.normal, in_disc1).normalize();
        let theta_v = theta.sin() * in_disc1 + theta.cos() * in_disc2;
        let p = self.center + r * theta_v;
        Sample{point: p, normal: self.normal}
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        let theta = 2.0 * PI * u;
        let r = v.sqrt() * self.radius;
        let in_disc1 = orth(&self.normal).normalize();
        let in_disc2 = cross(self.normal, in_disc1).normalize();
        let theta_v = theta.sin() * in_disc1 + theta.cos() * in_disc2;
        let p = self.center + r * theta_v;
        Sample{point: p, normal: self.normal}
    }

    fn get_area(&self) -> f32 {
        PI * self.radius_squared
    }
}