use crate::rt::bbox::BBox;
use crate::rt::mediums::medium::Medium;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::materials::material::Material;
use crate::core::point::Point;
use crate::rt::solids::solid::Solid;
use crate::rt::solids::solid::Sample;
use crate::rt::ray::Ray;
use crate::rt::intersection::Intersection;
use rand::thread_rng;
use rand::Rng;
use crate::core::vector::cross;
use crate::core::vector::dot;
use crate::core::vector::Vector;

pub struct SmoothTriangle {
    vertices: [Point; 3],
    normals: [Vector; 3],
    bbox: BBox,
    material: Option<Rc<Material>>,
    tex_mapper: Option<Rc<CoordMapper>>,
    id: u32,
    inside: Option<Rc<Medium>>,
    outside: Option<Rc<Medium>>,
    is_medium: bool
}


impl_primitive_fields!(SmoothTriangle);

impl SmoothTriangle {
    pub fn new(vertices: [Point; 3], normals: [Vector; 3], tex_mapper: Option<Rc<CoordMapper>>, material: Option<Rc<Material>>) -> SmoothTriangle {
        SmoothTriangle {
            vertices,
            normals,
            bbox: BBox::new(
                Point::new(f32::min(f32::min(vertices[0].x, vertices[1].x), vertices[2].x),
                           f32::min(f32::min(vertices[0].y, vertices[1].y), vertices[2].y),
                           f32::min(f32::min(vertices[0].z, vertices[1].z), vertices[2].z)),
                Point::new(f32::max(f32::max(vertices[0].x, vertices[1].x), vertices[2].x),
                           f32::max(f32::max(vertices[0].y, vertices[1].y), vertices[2].y),
                           f32::max(f32::max(vertices[0].z, vertices[1].z), vertices[2].z))
            ),
            material,
            tex_mapper,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false
        }
    }
}

impl Primitive for SmoothTriangle {
    fn get_bounds(&self) -> BBox {
        self.bbox
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        let u;
        let v;
        //Möller-Trumbore Algorithm

        let e01 = self.vertices[1] - self.vertices[0];
        let e02 = self.vertices[2] - self.vertices[0];
        let dp = cross(ray.d, e02);
        let determ = dot(e01, dp);
        if determ == 0.0 {
            return Intersection::failure();
        }
        let normal = cross(e01, e02).normalize();

        let ideterm = 1.0 / determ;


        let tp = ray.o - self.vertices[0];
        u = dot(tp, dp) * ideterm;
        if u < 0.0 || 1.0 < u { return Intersection::failure(); }

        let qp = cross(tp, e01);
        v = dot(ray.d, qp) * ideterm;
        if v < 0.0 || 1.0 < u + v { return Intersection::failure(); }

        let t = dot(e02, qp) * ideterm;

        if t >= 0.0 && t < previous_best_distance {
            let inters = Intersection::new(t, ray.clone(), self, normal, Point::new(1.0 - u - v, u, v));
            return Intersection::new(inters.distance, ray, self,
                                     self.normals[0] * inters.local().x + self.normals[1] * inters.local().y + self.normals[2] * inters.local().z,
                                     inters.local());
        }
        Intersection::failure()
    }
}

impl Solid for SmoothTriangle {
    fn sample(&self) -> Sample {
        let mut rng = thread_rng();
        let u: f32 = rng.gen();
        let v: f32 = rng.gen();
        let v1 = self.vertices[1] - self.vertices[0];
        let v2 = self.vertices[2] - self.vertices[0];
        let p =
            if u + v < 1.0 {
                self.vertices[0] + u * v1 + v * v2
            } else {
                self.vertices[0] + (1.0 - u) * v1 + (1.0 - v) * v2
            };

        Sample { point: p, normal: cross(v1, v2).normalize() }
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        self.sample()
    }

    fn get_area(&self) -> f32 {
        let a = self.vertices[1] - self.vertices[0];
        let b = self.vertices[2] - self.vertices[0];
        cross(a, b).length() / 2.0
    }
}