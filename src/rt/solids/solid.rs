use crate::core::point::Point;
use crate::core::vector::Vector;
use crate::rt::materials::material::Material;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::primitive::Primitive;


pub trait Solid: Primitive{
    fn sample(&self) -> Sample;

    fn sample_range(&self, u:f32, v:f32) -> Sample;

    fn get_area(&self) -> f32;
}

pub struct Sample{
    pub point: Point,
    pub normal: Vector
}

#[macro_export]
macro_rules! impl_primitive_fields{
    ($struct:ty) => {
        use crate::rt::primitive::*;
        use std::rc::Rc;
        impl PrimitiveFields for $struct{
            fn set_material(&mut self, m: Rc< Material>){
                self.material = Some(m);
            }

            fn get_material(&self) -> Option<Rc< Material>>{
                self.material.clone()
            }

            fn get_tex_mapper(&self) -> Option<Rc< CoordMapper>>{
                self.tex_mapper.clone()
            }

            fn set_tex_mapper(&mut self, cm: Rc< CoordMapper>){
                self.tex_mapper = Some(cm);
            }



            fn get_id(&self) -> u32{
                self.id
            }

            fn set_id(&mut self, id: u32){
                self.id = id;
            }

            fn get_counter(&self) -> u32{
                1
            }

            fn get_inside(&self) -> Option<Rc<Medium>>{
                self.inside.clone()
            }

            fn get_outside(&self) -> Option<Rc<Medium>>{
                self.outside.clone()
            }

            fn set_inside(&mut self, inside: Option<Rc< Medium>>){
                self.inside = inside;
            }

            fn set_outside(&mut self, outside: Option<Rc< Medium>>){
                self.outside = outside;
            }

            fn set_is_medium(&mut self, is:bool){
                self.is_medium = is;
            }

            fn get_is_medium(&self) -> bool{
                self.is_medium
            }
        }
    }
}