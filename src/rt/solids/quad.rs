use crate::core::vector::Vector;
use crate::rt::bbox::BBox;
use crate::rt::mediums::medium::Medium;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::materials::material::Material;
use crate::core::point::Point;
use crate::rt::solids::solid::Solid;
use crate::rt::solids::solid::Sample;
use crate::rt::ray::Ray;
use crate::rt::intersection::Intersection;
use crate::core::vector::cross;
use rand::thread_rng;
use rand::Rng;
use crate::rt::solids::triangle::Triangle;

pub struct Quad {
    v1: Point,
    span1: Vector,
    span2: Vector,
    tri1: Triangle,
    tri2: Triangle,
    bbox: BBox,
    material: Option<Rc<Material>>,
    tex_mapper: Option<Rc<CoordMapper>>,
    id: u32,
    inside: Option<Rc<Medium>>,
    outside: Option<Rc<Medium>>,
    is_medium: bool
}

impl Quad{
    pub fn new(v1: Point, span1: Vector, span2: Vector, tex_mapper: Option<Rc<CoordMapper>>, material: Option<Rc<Material>>) -> Quad{
        let v2 = v1 + span1;
        let v3 = v1 + span2;
        let v4 = v2 + span2;
        Quad{
            v1,
            span1,
            span2,
            tri1: Triangle::new_points(v2, v3, v1, None, None),
            tri2: Triangle::new_points(v2, v3, v4, None, None),
            bbox: BBox::new(
                Point::new(f32::min(f32::min(v1.x, v2.x), f32::min(v3.x, v4.x)),
                      f32::min(f32::min(v1.y, v2.y), f32::min(v3.y, v4.y)),
                      f32::min(f32::min(v1.z, v2.z), f32::min(v3.z, v4.z))
                ),
                Point::new(f32::max(f32::max(v1.x, v2.x), f32::max(v3.x, v4.x)),
                      f32::max(f32::max(v1.y, v2.y), f32::max(v3.y, v4.y)),
                      f32::max(f32::max(v1.z, v2.z), f32::max(v3.z, v4.z))
                )),
            material,
            tex_mapper,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false
        }
    }
}

impl_primitive_fields!(Quad);

impl Primitive for Quad{
    fn get_bounds(&self) -> BBox {
        self.bbox
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        //cheat by treating the quad as 2 Triangles
        let intersection1 = self.tri1.intersect(ray.clone(), previous_best_distance);
        if !intersection1.failure && intersection1.distance >= 0.0 && intersection1.distance < previous_best_distance {
            let tribay = intersection1.local();
            return Intersection::new(intersection1.distance, ray, self,
                                     cross(self.span1, self.span2).normalize(), Point::new(tribay.x, tribay.y, 0.0));
        }


        let intersection2 = self.tri2.intersect(ray.clone(), previous_best_distance);

        if !intersection2.failure && intersection1.distance >= 0.0 && intersection1.distance < previous_best_distance {
            let tribay = intersection2.local();
            return Intersection::new(intersection2.distance, ray, self,
                                     cross(self.span1, self.span2).normalize(),
                                Point::new(tribay.x + tribay.z, tribay.y + tribay.z, 0.0)
            );

        }


        Intersection::failure()
    }
}

impl Solid for Quad{
    fn sample(&self) -> Sample {
        let mut rng = thread_rng();
        let u:f32 = rng.gen();
        let v:f32 = rng.gen();
        let p = self.v1 + u * self.span1 + v * self.span2;
        Sample{point: p, normal: cross(self.span1, self.span2).normalize()}
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        self.sample()
    }

    fn get_area(&self) -> f32 {
        cross(self.span1, self.span2).length()
    }
}