#[macro_use]
pub mod solid;
pub mod aabox;
pub mod disc;
pub mod environment_mapper_solid;
pub mod infinite_plane;
pub mod quad;
pub mod quadric;

pub mod sphere;
pub mod striangle;
pub mod triangle;