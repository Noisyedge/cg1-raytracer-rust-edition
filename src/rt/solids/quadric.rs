use crate::rt::mediums::medium::Medium;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::materials::material::Material;
use crate::core::point::Point;
use crate::core::vector::Vector;
use crate::rt::bbox::BBox;
use crate::rt::solids::solid::Solid;
use crate::rt::ray::Ray;
use crate::rt::intersection::Intersection;
use crate::rt::solids::solid::Sample;
use std::mem::swap;
use crate::core::scalar::midnight_formula;
use num_traits::Float;

pub struct Quadric {
    q: [f32; 10],
    center: Point,
    rot_xy: f32,
    rot_xz: f32,
    rot_yz: f32,
    material: Option<Rc<Material>>,
    tex_mapper: Option<Rc<CoordMapper>>,
    id: u32,
    inside: Option<Rc<Medium>>,
    outside: Option<Rc<Medium>>,
    is_medium: bool
}

impl Quadric {
    pub fn new(q: [f32; 10], center: Point, rot_xy: f32, rot_xz: f32, rot_yz: f32, tex_mapper: Option<Rc<CoordMapper>>, material: Option<Rc<Material>>) -> Quadric {
        Quadric {
            q,
            center,
            rot_xy,
            rot_xz,
            rot_yz,
            material,
            tex_mapper,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false
        }
    }
}

impl_primitive_fields!(Quadric);

#[inline]
fn sqr(x: f32) -> f32{
    x * x
}

impl Primitive for Quadric {
    fn get_bounds(&self) -> BBox {
        BBox::full()
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        let d = ray.d;
        let o = Point::new(ray.o.x - self.center.x, ray.o.y - self.center.y, ray.o.z - self.center.z);

        let a = self.q[0] * sqr(d.x) + self.q[1] * sqr(d.y) + self.q[2] * sqr(d.z) + self.q[3] * d.x * d.y +
            self.q[4] * d.x * d.z + self.q[5] * d.y * d.z;
        let b = 2.0 * (self.q[0] * o.x * d.x + self.q[1] * o.y * d.y + self.q[2] * o.z * d.z) +
            self.q[3] * o.x * d.y + self.q[3] * o.y * d.x + self.q[4] * o.x * d.z +
            self.q[4] * o.z * d.x + self.q[5] * o.y * d.z + self.q[5] * o.z * d.y +
            self.q[6] * d.x + self.q[7] * d.y + self.q[8] * d.z;

        let c = self.q[0] * sqr(o.x) + self.q[1] * sqr(o.y) + self.q[2] * sqr(o.z) + self.q[3] * o.x * o.y +
            self.q[4] * o.x * o.z + self.q[5] * o.y * o.z + self.q[6] * o.x + self.q[7] * o.y
            + self.q[8] * o.z + self.q[9];

        let mut t0 = 0.0;
        let mut t1 = 0.0;


        if a > 1e-8 {
            if !midnight_formula(a, b, c, &mut t0, &mut t1) { return Intersection::failure(); }
        } else {
            t0 = -c / b;
            t1 = t0;
        }
        if t0 > t1 { swap(&mut t0, &mut t1); }


        if t0 < 0.0 {
            t0 = t1;
            if t1 < 0.0 {
                return Intersection::failure();
            } // both t0 and t1 are negative
        }

        if t1 > previous_best_distance {
            t1 = t0;
            if t1 > previous_best_distance{
            return Intersection::failure();}
        }


        let hit = o + (t0 * d);
        let normal = Vector::new(
            2.0 * self.q[0] * hit.x + self.q[3] * hit.y + self.q[4] * hit.z + self.q[6],
            self.q[3] * hit.x + 2.0 * self.q[1] * hit.y + self.q[5] * hit.z + self.q[7],
            self.q[4] * hit.x + self.q[5] * hit.y + 2.0 * self.q[2] * hit.z + self.q[8]
        ).normalize();

       Intersection::new(t0, ray, self, normal.normalize(), hit)



    }
}

impl Solid for Quadric {
    fn sample(&self) -> Sample {
        unimplemented!()
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        unimplemented!()
    }

    fn get_area(&self) -> f32 {
        f32::infinity()
    }
}