use crate::core::point::Point;
use crate::rt::mediums::medium::Medium;
use crate::rt::materials::material::Material;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::solids::solid::Solid;
use crate::rt::solids::solid::Sample;
use crate::rt::ray::Ray;
use crate::rt::bbox::BBox;
use crate::rt::intersection::Intersection;
use std::f32::consts::PI;
use crate::core::vector::dot;
use crate::core::scalar::midnight_formula;
use std::mem::swap;

pub struct Sphere {
    center: Point,
    radius: f32,
    radius_squared: f32,
    bbox: BBox,
    material: Option<Rc<Material>>,
    tex_mapper: Option<Rc<CoordMapper>>,
    id: u32,
    inside: Option<Rc<Medium>>,
    outside: Option<Rc<Medium>>,
    is_medium: bool
}

impl Sphere {
    pub fn new(center: Point, radius: f32, tex_mapper: Option<Rc<CoordMapper>>, material: Option<Rc<Material>>) -> Sphere {
        Sphere {
            center,
            radius,
            radius_squared: radius * radius,
            material,
            bbox: BBox::new(
                Point::new(center.x - radius, center.y - radius, center.z - radius),
                Point::new(center.x + radius, center.y + radius, center.z + radius)
            ),
            tex_mapper,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false
        }
    }
}

impl_primitive_fields!(Sphere);

impl Primitive for Sphere {
    fn get_bounds(&self) -> BBox {
        self.bbox
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        let mut t0 = 0.0;
        let mut t1 = 0.0;


        let a = ray.d.lensqr();

        let OTS = ray.o - self.center;

        let b = 2.0 * dot(ray.d, OTS);


        let c = OTS.lensqr() - self.radius_squared;

        if !midnight_formula(a, b, c, &mut t0, &mut t1) {
            return Intersection::failure();
        }

        if t0 > t1 {
            swap(&mut t0, &mut t1);
        }


        if t0 < 0.0 {
            t0 = t1;
            if t1 < 0.0 {
                return Intersection::failure();
            } // both t0 and t1 are negative
        }

        if t1 > previous_best_distance {
            t1 = t0;
            if t1 > previous_best_distance {
                return Intersection::failure();
            }
        }


        let hit = ray.o + (t0 * ray.d);

        let normal = hit - self.center;


     Intersection::new(t0, ray, self, normal.normalize(), hit)



    }
}

impl Solid for Sphere {
    fn sample(&self) -> Sample {
        unimplemented!()
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        unimplemented!()
    }

    fn get_area(&self) -> f32 {
        4.0 * PI * self.radius_squared
    }
}