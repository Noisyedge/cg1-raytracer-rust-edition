use crate::rt::bbox::BBox;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::materials::material::Material;
use crate::rt::solids::solid::Solid;
use crate::rt::solids::solid::Sample;
use crate::rt::ray::Ray;
use crate::core::vector::Vector;
use crate::core::point::Point;
use crate::rt::mediums::medium::Medium;
use crate::rt::intersection::Intersection;
use num_traits::Float;
use crate::core::vector::dot;

pub struct InfinitePlane {
    origin: Point,
    normal: Vector,
    bbox: BBox,
    material: Option<Rc<Material>>,
    tex_mapper: Option<Rc<CoordMapper>>,
    id: u32,
    inside: Option<Rc<Medium>>,
    outside: Option<Rc<Medium>>,
    is_medium: bool
}

impl InfinitePlane {
    pub fn new(origin: Point, normal: Vector, tex_mapper: Option<Rc<CoordMapper>>, material: Option<Rc<Material>>) -> InfinitePlane {
        let maxf = f32::infinity();
        let minf = f32::neg_infinity();
        let bbox;
        if normal.x == normal.y && normal.y == 0.0 {
            bbox = BBox::new(Point::new(minf, minf, origin.z), Point::new(maxf, maxf, origin.z));
        } else if normal.y == normal.z && normal.z == 0.0 {
            bbox = BBox::new(Point::new(origin.x, minf, minf), Point::new(origin.x, maxf, maxf));
        } else if normal.x == normal.z && normal.z == 0.0 {
            bbox = BBox::new(Point::new(minf, origin.y, minf), Point::new(maxf, origin.y, maxf));
        } else {
            bbox = BBox::new(Point::rep(minf), Point::rep(maxf));
        }

        InfinitePlane {
            origin,
            normal,
            bbox,
            material,
            tex_mapper,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false
        }
    }
}

impl_primitive_fields!(InfinitePlane);

impl Primitive for InfinitePlane {
    fn get_bounds(&self) -> BBox {
        self.bbox
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        let desc = dot(-ray.d, self.normal);
        let localvec = ray.o - self.origin;
        let t = dot(localvec, self.normal) / desc;
        if 0.0 > t || t > previous_best_distance {
            return Intersection::failure();
        }
        let hit = ray.o + (t * ray.d);
        Intersection::new(t, ray, self, self.normal.normalize(), hit)
    }
}

impl Solid for InfinitePlane {
    fn sample(&self) -> Sample {
        unimplemented!()
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        unimplemented!()
    }

    fn get_area(&self) -> f32 {
        f32::infinity()
    }
}