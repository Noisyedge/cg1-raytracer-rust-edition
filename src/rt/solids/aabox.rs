use crate::core::point::Point;
use crate::rt::bbox::BBox;
use crate::rt::materials::material::Material;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use num_traits::real::Real;
use crate::rt::primitive::Primitive;
use crate::rt::mediums::medium::Medium;
use crate::rt::ray::Ray;
use crate::rt::intersection::Intersection;
use std::mem::swap;
use crate::core::vector::Vector;
use crate::rt::solids::solid::Solid;
use crate::rt::solids::solid::Sample;

pub struct AABox {
    min_corner: Point,
    max_corner: Point,
    bbox: BBox,
    material: Option<Rc< Material>>,
    tex_mapper: Option<Rc< CoordMapper>>,
    id: u32,
    inside: Option<Rc< Medium>>,
    outside: Option<Rc< Medium>>,
    is_medium: bool
}

impl_primitive_fields!(AABox);

impl AABox {
    pub fn new(corner1: Point, corner2: Point, tex_mapper: Option<Rc< CoordMapper>>, material: Option<Rc< Material>>) -> AABox {
        let min_corner = corner1.min(corner2);
        let max_corner = corner2.max(corner2);
        AABox {
            min_corner,
            max_corner,
            bbox: BBox::new(min_corner, max_corner),
            tex_mapper,
            material,
            inside: None,
            outside: None,
            is_medium: false,
            id: 0
        }
    }
}



impl Primitive for AABox {
    fn get_bounds(&self) -> BBox {
        self.bbox
    }

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection {
        let mut tmin;
        let mut tmax;
        let mut enter ;
        let mut mintomaxx = true;
        let mut mintomaxy = true;
        let mut mintomaxz = true;


        let mut tminx = (self.min_corner.x - ray.o.x) / ray.d.x;
        let mut tmaxx = (self.max_corner.x - ray.o.x) / ray.d.x;

        if tminx > tmaxx {
            swap(&mut tminx, &mut tmaxx);
            mintomaxx = false;
        }


        tmin = tminx;
        tmax = tmaxx;
        enter = 'x';


        let mut tminy = (self.min_corner.y - ray.o.y) / ray.d.y;
        let mut tmaxy = (self.max_corner.y - ray.o.y) / ray.d.y;

        if tminy > tmaxy {
            swap(&mut tminy, &mut tmaxy);
            mintomaxy = false;
        }

        if tmin > tmaxy || tminy > tmax {
            return Intersection::failure();
        }


        if tminy > tmin {
            tmin = tminy;
            enter = 'y';
        }
        if tmaxy < tmax {
            tmax = tmaxy;
        }

        let mut tminz = (self.min_corner.z - ray.o.z) / ray.d.z;
        let mut tmaxz = (self.max_corner.z - ray.o.z) / ray.d.z;

        if tminz > tmaxz {
            swap(&mut tminz, &mut tmaxz);
            mintomaxz = false;
        }

        if tmin > tmaxz || tminz > tmax {
            return Intersection::failure();
        }
        if tminz > tmin {
            tmin = tminz;
            enter = 'z';
        }
        if 0.0 > tmin || tmin > previous_best_distance { return Intersection::failure(); }

        if tmaxz < tmax {
            tmax = tmaxz;
        }


        let hit = ray.o + (tmin * ray.d);

        let normal;
        //For the sake of simplicity, we leave compensation for diagonal normals on the Edges
        //and Corners to the Shading algorythm

        match enter {
            'x' =>
                if mintomaxx {
                    normal = Vector::new(-1.0, 0.0, 0.0);
                } else {
                    normal = Vector::new(1.0, 0.0, 0.0);
                }

            'y' =>
                if mintomaxy {
                    normal = Vector::new(0.0, -1.0, 0.0);
                } else {
                    normal = Vector::new(0.0, 1., 0.0);
                }

            'z' =>
                if mintomaxz {
                    normal = Vector::new(0., 0., -1.);
                } else {
                    normal = Vector::new(0., 0., 1.);
                }

            _ => {
                unreachable!();
            }
        }

        Intersection::new(tmin, ray, self, normal, hit)
    }


}

impl Solid for AABox{
    fn sample(&self) -> Sample {
        unimplemented!()
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        unimplemented!()
    }

    fn get_area(&self) -> f32 {
        let diag = self.max_corner - self.min_corner;
        2.0 * (diag.x * diag.y + diag.x * diag.z + diag.y * diag.z)
    }
}