use crate::rt::mediums::medium::Medium;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use crate::rt::materials::material::Material;
use crate::rt::solids::solid::Solid;
use crate::rt::bbox::BBox;
use crate::rt::solids::solid::Sample;
use crate::rt::ray::Ray;
use crate::rt::intersection::Intersection;
use num_traits::Float;
use crate::core::point::Point;

pub struct EnvironmentMapperSolid{
    material: Option<Rc< Material>>,
    tex_mapper: Option<Rc< CoordMapper>>,
    id: u32,
    inside: Option<Rc< Medium>>,
    outside: Option<Rc< Medium>>,
    is_medium: bool
}

impl EnvironmentMapperSolid{
    pub fn new(tex_mapper: Option<Rc<CoordMapper>>, material: Option<Rc<Material>>) -> EnvironmentMapperSolid{
        EnvironmentMapperSolid{
            material,
            tex_mapper,
            id: 0,
            inside: None,
            outside: None,
            is_medium: false
        }
    }
}

impl_primitive_fields!(EnvironmentMapperSolid);

impl Primitive for EnvironmentMapperSolid{
    fn get_bounds(&self) -> BBox {
        BBox::full()
    }

    fn intersect(&self, ray: Ray, _previous_best_distance: f32) -> Intersection {
        Intersection::new(f32::max_value(), ray.clone(), self, -ray.d, Point::new(ray.d.x, ray.d.y, ray.d.z))
    }
}

impl Solid for EnvironmentMapperSolid{
    fn sample(&self) -> Sample {
        unimplemented!()
    }

    fn sample_range(&self, u: f32, v: f32) -> Sample {
        unimplemented!()
    }

    fn get_area(&self) -> f32 {
        f32::infinity()
    }
}
