use crate::rt::bbox::BBox;
use crate::rt::ray::Ray;
use crate::rt::intersection::Intersection;
use crate::rt::mediums::medium::Medium;
use crate::rt::materials::material::Material;
use crate::rt::coordmappers::coordmapper::CoordMapper;
use std::rc::Rc;

pub trait PrimitiveFields {
    fn set_material(&mut self, m: Rc<Material>);

    fn get_material(&self) -> Option<Rc<Material>>;

    fn get_tex_mapper(&self) -> Option<Rc<CoordMapper>>;

    fn set_tex_mapper(&mut self, cm: Rc<CoordMapper>);


    fn get_id(&self) -> u32;

    fn set_id(&mut self, id: u32);

    fn get_counter(&self) -> u32;

    fn get_inside(&self) -> Option<Rc<Medium>>;

    fn get_outside(&self) -> Option<Rc<Medium>>;

    fn set_inside(&mut self, inside: Option<Rc<Medium>>);

    fn set_outside(&mut self, outside: Option<Rc<Medium>>);

    fn set_is_medium(&mut self, is: bool);

    fn get_is_medium(&self) -> bool;
}

pub trait Primitive: PrimitiveFields {
    fn get_bounds(&self) -> BBox;

    fn intersect(&self, ray: Ray, previous_best_distance: f32) -> Intersection;
}