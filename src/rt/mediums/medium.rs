use std::rc::Rc;

pub trait Medium{

}

#[derive(Clone)]
pub struct MediumInterface{

    pub inside: Rc<Medium>,
    pub outside: Rc<Medium>

}

impl MediumInterface{
    pub fn new_boundary(inside: Rc<Medium>, outside: Rc<Medium>) -> MediumInterface{
        MediumInterface{
            inside,
            outside
        }
    }

    pub fn new(medium: Rc<Medium>) -> MediumInterface{
        MediumInterface {
            inside: medium.clone(),
            outside: medium
        }
    }

    pub fn is_medium_transition(&self) -> bool{
        self.inside.as_ref() as *const _ == self.outside.as_ref() as *const _
    }
}


