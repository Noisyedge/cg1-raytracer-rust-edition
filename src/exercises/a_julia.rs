use crate::core::point::Point;
use crate::core::julia::julia;
use crate::core::color::RGBColor;
use crate::core::image::Image;
use crate::rt::renderer::Renderer;
use crate::rt::cameras::orthographicc::OrthographicCamera;
use crate::rt::integrators::casting::CastingIntegrator;
use crate::core::vector::Vector;
use crate::rt::world::World;
use crate::rt::solids::sphere::Sphere;
use crate::rt::integrators::dummy::DummyIntegrator;

pub fn a1_compute_weight(fx: f32, fy: f32, c: Point, div: f32) -> f32 {
    let mut v = Point::new(fx, fy, 0.5);
    v = v - Vector::rep(0.5);
    v = v * 2.0;
    let num_iter = julia(v, c);
    num_iter as f32 / (num_iter as f32 + div)
}

pub fn a1_compute_color(x: u32, y: u32, width: u32, height: u32) -> RGBColor {
    let fx = x as f32 / width as f32;
    let fy = y as f32 / height as f32;
    let mut color = RGBColor::rep(0.0);
    color = color + a1_compute_weight(fx, fy, Point::new(-0.8, 0.156, 0.0), 64.0) * RGBColor::new(0.8, 0.8, 1.0);
    color = color + a1_compute_weight(fx, fy, Point::new(-0.6, 0.2, 0.0), 64.0) * 0.2 * RGBColor::new(0.5, 0.5, -0.2);
    color = color + a1_compute_weight(fy, fx, Point::new(0.285, 0.0, 0.0), 64.0) * RGBColor::new(0.2, 0.3, 0.4);
    RGBColor::rep(1.0) - color
}

pub fn a_julia() {
    let mut img = Image::new(800, 800);
    let cam = OrthographicCamera::new(Point::rep(0.0),
                                      Vector::rep(0.0), Vector::rep(0.0),
                                      0.0, 0.0, None);
    let engine = Renderer::new_ss(&cam, &DummyIntegrator{});
    engine.test_render1(&mut img);
    img.save_png("a1.png");
}

