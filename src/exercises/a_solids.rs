use crate::core::image::Image;
use crate::rt::renderer::Renderer;
use crate::core::color::RGBColor;
use crate::rt::cameras::perspective::PerspectiveCamera;
use crate::rt::solids::aabox::AABox;
use crate::rt::solids::disc::Disc;
use crate::rt::solids::quad::Quad;
use crate::rt::solids::triangle::Triangle;
use crate::rt::solids::infinite_plane::InfinitePlane;
use crate::rt::solids::sphere::Sphere;
use crate::core::point::Point;
use crate::core::vector::Vector;
use crate::rt::world::World;
use std::f32::consts::PI;
use crate::rt::groups::simplegroup::SimpleGroup;
use crate::rt::integrators::casting::CastingIntegrator;
use crate::rt::integrators::castingdist::CastingDistIntegrator;
use crate::rt::groups::group::Group;
use crate::rt::primitive::Primitive;


pub fn a_solids() {
    let mut img = Image::new(640, 480);

    let mut prim: Vec<Box<Primitive>> = Vec::new();
    prim.push(Box::new(Sphere::new(Point::new(-2.0, 1.7, 0.0), 2.0, None, None)));
    prim.push(Box::new(Sphere::new(Point::new(1.0, -1.0, 1.0), 2.2, None, None)));
    prim.push(Box::new(Sphere::new(Point::new(3.0, 0.8, -2.0), 2.0, None, None)));

    prim.push(Box::new(InfinitePlane::new(Point::new(0.0, -1.0, 0.0), Vector::new(0.0, 1.0, 0.0), None, None)));

    prim.push(Box::new(Triangle::new_points(Point::new(-2.0, 3.7, 0.0), Point::new(1.0, 2.0, 1.0), Point::new(3.0, 2.8, -2.0), None, None)));
    prim.push(Box::new(Triangle::new_points(Point::new(3.0, 2.0, 3.0), Point::new(3.0, 2.0, -3.0), Point::new(-3.0, 2.0, -3.0), None, None)));
    prim.push(Box::new(Quad::new(Point::new(1.0, -0.9, 4.5), Vector::new(-2.0, 0.0, 0.0), Vector::new(0.0, 0.1, -2.0), None, None)));

    prim.push(Box::new(Disc::new(Point::new(-3.0, -0.75, 1.5), Vector::new(0.0, 0.5, 0.5), 1.5, None, None)));

    prim.push(Box::new(AABox::new(Point::new(2.0, 1.5, -0.5), Point::new(3.0, 2.5, 2.5), None, None)));
    let mut scene = SimpleGroup::new();
    for p in &mut prim {
        scene.add(p.as_mut());
    }


    let world = World::new(&scene);

    let cam = PerspectiveCamera::new(Point::new(0.0, 0.0, 10.0), Vector::new(0.0, 0.0, -1.0), Vector::new(0.0, 1.0, 0.0), PI / 4.0, PI / 3.0, None);


    let integrator = CastingIntegrator::new(&world);
    let mut engine = Renderer::new_ss(&(cam), &(integrator));
    engine.render(&mut img);
    img.save_png("a2-1.png");

    let integratorb = CastingDistIntegrator::new(&world, RGBColor::new(1.0, 0.2, 0.0), 4.0, RGBColor::new(0.2, 1.0, 0.0), 12.0);
    let mut engineb = Renderer::new_ss(&(cam), &(integratorb));
    engineb.render(&mut img);
    img.save_png("a2-2.png");
}