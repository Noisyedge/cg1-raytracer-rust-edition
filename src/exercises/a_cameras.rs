use crate::rt::renderer::Renderer;
use crate::core::vector::Vector;
use crate::rt::cameras::perspective::PerspectiveCamera;
use crate::core::image::Image;
use crate::core::color::RGBColor;
use crate::rt::ray::Ray;
use crate::core::julia::julia;
use crate::core::point::Point;
use std::f32::consts::PI;
use crate::core::scalar::abs_fractional;
use crate::rt::integrators::casting::CastingIntegrator;
use crate::rt::cameras::orthographicc::OrthographicCamera;
use crate::rt::solids::sphere::Sphere;
use crate::rt::world::World;
use crate::rt::integrators::dummy::DummyIntegrator;

pub fn a2_compute_weight(fx: f32, fy: f32, c: Point, div: f32) -> f32 {
    let v = Point::new(fx, fy, 0.0);
    let num_iter = julia(v, c);
    num_iter as f32 / (num_iter as f32 + div)
}

pub fn a2_compute_color(r: Ray) -> RGBColor {
    let theta = r.d.z.asin() / PI * 2.0;
    let phi = f32::atan2(r.d.y, r.d.x) / PI;
    let ofx = abs_fractional((r.o.x + 1.0) / 2.0) * 2.0 - 1.0;
    let ofy = abs_fractional((r.o.y + 1.0) / 2.0) * 2.0 - 1.0;
    let mut color = RGBColor::rep(0.0);
    color = color + a2_compute_weight(phi, theta, Point::new(-0.8, 0.156, 0.0), 64.0) * RGBColor::new(0.8, 0.8, 1.0);
    color = color +
        a2_compute_weight(phi, theta, Point::new(-0.6, 0.2, 0.0), 64.0) * 0.2 * RGBColor::new(0.5, 0.5, -0.2);
    color = color + a2_compute_weight(ofy, ofx, Point::new(0.285, 0.0, 0.0), 64.0) * RGBColor::new(0.4, 0.5, 0.6);
    color = RGBColor::rep(1.0) - color;
    if abs_fractional(theta / (2.0 * PI) * 90.0) < 0.03 { color = RGBColor::new(0.9, 0.5, 0.5) * 0.7; }
    if abs_fractional(phi / (2.0 * PI) * 90.0) < 0.03 { color = RGBColor::new(0.9, 0.9, 0.5) * 0.7; }
    color
}

pub fn a_cameras() {
    let mut img = Image::new(800, 800);
    let mut low = Image::new(128, 128);

    let pcam = PerspectiveCamera::new(Point::new(0.0, 0.0, 0.0), Vector::new(1.0, 0.0, 0.1), Vector::new(0.0, 0.0, 1.0), PI / 3.0, PI / 3.0, None);
    let r1 = Renderer::new_ss(&pcam, &DummyIntegrator{});
    r1.test_render2(&mut img);
    r1.test_render2(&mut low);
    img.save_png("a1-2.png");
    low.save_png("a1-2-low.png");

    let pcam2 = PerspectiveCamera::new(Point::new(0.0, 0.0, 0.0), Vector::new(0.5, 0.5, 0.3), Vector::new(0.0, 0.0, 1.0), PI * 0.9, PI * 0.9, None);
    let r12 = Renderer::new_ss(&pcam2, &DummyIntegrator{});
    r12.test_render2(&mut img);
    img.save_png("a1-3.png");

    let ocam = OrthographicCamera::new(Point::new(0.0, 0.0, 0.0), Vector::new(0.1, 0.1, 1.0), Vector::new(0.2, 1.0, 0.2), 10.0, 10.0, None);
    let r2 = Renderer::new_ss(&(ocam), &(DummyIntegrator{}));
    r2.test_render2(&mut img);
    img.save_png("a1-4.png");
}


